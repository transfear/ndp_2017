﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Simulation_NDP2016.Data;
using FrameworkInterface.Player;
using Maths;

namespace ExamplePlayers
{
    public class ExamplePlayer : IPlayer
    {
        public ExamplePlayer() { }

        float lastValidTime;
        float lastDeltaTime;
        float maxDeltaTime = 0.1f;

        public override string GetPlayerName()
        {
            return "Example player";
        }

        public override IPlayerOutput OnGameStart(IPlayerInput _input)
        {
            PlayerInput simInput = _input as PlayerInput;
            if (simInput == null)
                return null;

            lastValidTime = simInput.track.StartLine;
            lastDeltaTime = 1.00f;

            return null;
        }

        public override IPlayerOutput Update(IPlayerInput _input)
        {
            PlayerInput simInput = _input as PlayerInput;
            if (simInput == null)
                return null;

            PlayerInput.PlayerData myData = simInput.playerData;

            float fSteering = 0.0f;
            float time = simInput.track.CenterLine.GetClosestTimeToPoint(myData.vPosition);
            if ((Math.Abs(time - lastValidTime) > (maxDeltaTime)) && // Handle large delta which should happen, most probably passing through a crossing
                !(lastValidTime + lastDeltaTime >= 1.0f && time < maxDeltaTime)) // Handle lap completed (last time near 1.0f and new time near 0.0f)
            {
                time = lastValidTime + lastDeltaTime;
                lastValidTime = time;
            }
            else
            {
                lastDeltaTime = Math.Abs(time - lastValidTime);
                lastValidTime = time;
            }

            Vector2 pos = simInput.track.CenterLine.Eval(time);
            Vector2 tan = simInput.track.CenterLine.EvalTangent(time);
            Vector2 dir = simInput.playerData.vDirection;

            dir.Normalize();
            tan.Normalize();

            //iterate along the spline path
            for (int i = 1; i <= 2; i++)
            {
                time = simInput.track.CenterLine.GetClosestTimeToPoint(pos + tan * (i * 10.0f));
                pos = simInput.track.CenterLine.Eval(time);
                tan = simInput.track.CenterLine.EvalTangent(time);
            }

            Vector2 vecDebug = new Vector2(pos.X - myData.vPosition.X, pos.Y - myData.vPosition.Y);
            Vector2 vecToPos = new Vector2(vecDebug.X, vecDebug.Y);
            vecToPos.Normalize();

            Vector2 carRight = new Vector2(myData.vDirection.Y, myData.vDirection.X * -1.0f);
            carRight.Normalize();

            if (Vector2.Dot(vecToPos, myData.vDirection) < 0.98f)
            {
                float cosAngle = Vector2.Dot(vecToPos, carRight);
                fSteering = cosAngle < 0 ? 0.25f : -0.25f;
            }

            PlayerOutput toReturn = new PlayerOutput();

            toReturn.fThrottle = 1.0f;
            toReturn.fBrake = 0.0f;
            toReturn.fSteering = fSteering;
            toReturn.bShotProjectile = myData.uiAmmo > 0;

            //demo code, you may want to use these functions do to something smart///////////////////////////////////
            Line raycastDemo = new Line(myData.vPosition, pos);
            Vector2 raycastResult = new Vector2();
            //Raycast track border
            simInput.track.Raycast(raycastDemo, out raycastResult);

            //Raycast track object           
            simInput.track.Raycast(TrackObjectType.Bounce | TrackObjectType.PowerUp | TrackObjectType.Boost, simInput.objects, raycastDemo, out raycastResult);
            bool offRoad = simInput.track.IsOnGrass(raycastResult);

            //debug draw 
            toReturn.DebugLines.Add(new Line(myData.vPosition, vecDebug + myData.vPosition));
            toReturn.DebugLines.Add(new Line(myData.vPosition, carRight * -1.0f * 20.0f + myData.vPosition)); //Reminder, cartesian vector != image vector. Need to invert rotation to have correct image position
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////

            return toReturn;
        }

        public override IPlayerOutput OnGameEnd(IPlayerInput _input)
        {
            return null;
        }
    }
}
