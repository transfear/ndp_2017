﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Simulation_NDP2016.Data;

using FrameworkInterface.Player;
using FrameworkInterface.Renderer;
using FrameworkInterface.Simulation;
using System.Windows;

using Maths;

namespace Simulation_NDP2016.Internal
{
    class BaseSimulation
    {
        #region Public Interface
        public ICollection<IPlayer> PlayerList { get; set; }
        public IPlayer CurrentPlayer { get; set; }

        Stopwatch stopwatch = null;
        float fDeltaT = 1.0f / 30.0f;
        const float fObjCooldownInSec = 3.0f;
		const float fBoosterVelocityMultiplier = 2.0f;
		const float fProjectileSpeed = 250.0f;

        class CarData
        {
            public const float fEngineForce = 10000; //rpm - input param
            public const float fBrakeScale = 0.75f;
            public const float fCarMass = 125;  //kg
            public const float fCarLength = 2.0f;
            public static Vector2 carSize = new Vector2(8.0f, 12.0f);

            public Vector2 oldPosition = new Vector2();
            public Vector2 carPosition = new Vector2();
            public Vector2 carDirection = new Vector2();
            public Vector2 carVelocity = new Vector2();

            public TimeSpan lastDeltaT = new TimeSpan(0);
            public Stopwatch timer = new Stopwatch();

            public Stopwatch stunTimer = new Stopwatch();

			public List<int> lapSegments = new List<int>();
			public int currentLap = 0;
			public uint uiAmmo = 0;

            public float fLastSteering = 0.0f;

			public Box boundingBox = new Box();

            public CarData()
            {
                // Start the Stopwatch as soon as object is created
                timer.Start();
            }
        }

        public void OnGameStart(ICollection<IPlayer> _playerCollection)
        {
            PlayerList = _playerCollection;
            Init();

            stopwatch = new Stopwatch();
            stopwatch.Start();
        }

        public IPlayerInput ProducePlayerInput(IPlayer _player)
        {
            CarData curPlayer = mCarDataMap[_player];

            PlayerInput.PlayerData playerData = new PlayerInput.PlayerData();
            playerData.uiPlayerIndex = _player.PlayerIndex;
            playerData.vPosition = curPlayer.carPosition;
            playerData.vDirection = curPlayer.carDirection;
            playerData.vVelocity = curPlayer.carVelocity;
			playerData.uiAmmo = curPlayer.uiAmmo;
			playerData.boundingBox = curPlayer.boundingBox;

			PlayerInput toReturn = new PlayerInput();
            toReturn.playerData = playerData;
            toReturn.objects = mObstacles;
            toReturn.projectiles = mProjectiles.ToArray();
            toReturn.track = mTrack;
            toReturn.deltaT = curPlayer.lastDeltaT;
			toReturn.boosterVelocityMultiplier = fBoosterVelocityMultiplier;
			toReturn.projectileSpeed = fProjectileSpeed;
			toReturn.otherPlayerData = new PlayerInput.PlayerData[mCarDataMap.Count - 1]; //Not the player

			// Get other player's data
			int otherPlayerIdx = 0;
			foreach (KeyValuePair<IPlayer, CarData> otherPlayer in mCarDataMap)
			{
				if (otherPlayer.Key.PlayerIndex == _player.PlayerIndex)
					continue;

				PlayerInput.PlayerData otherPlayerData = new PlayerInput.PlayerData();
				otherPlayerData.uiPlayerIndex = otherPlayer.Key.PlayerIndex;
				otherPlayerData.vPosition = otherPlayer.Value.carPosition;
				otherPlayerData.vDirection = otherPlayer.Value.carDirection;
				otherPlayerData.vVelocity = otherPlayer.Value.carVelocity;
				otherPlayerData.uiAmmo = otherPlayer.Value.uiAmmo;
				otherPlayerData.boundingBox = otherPlayer.Value.boundingBox;

				toReturn.otherPlayerData[otherPlayerIdx++] = otherPlayerData;
			}

			return toReturn;
        }

        public void ApplyPlayerOuput(IPlayer _player, IPlayerOutput _playerResponse)
        {
            PlayerOutput response = _playerResponse as PlayerOutput;
            if (response == null)
                return;

            // We just received a response from a player, reset its delta T
            CarData playerCarData = mCarDataMap[_player];
            playerCarData.lastDeltaT = playerCarData.timer.Elapsed;
            playerCarData.timer.Restart();

            // Validate response
            response.fThrottle = Math.Min(Math.Max(response.fThrottle, 0.0f), 1.0f);
            response.fBrake = Math.Min(Math.Max(response.fBrake, 0.0f), 1.0f);
            response.fSteering = Math.Min(Math.Max(response.fSteering, -1.0f), 1.0f);

            if (float.IsInfinity(response.fThrottle) || float.IsNaN(response.fThrottle))
                response.fThrottle = 0.0f;

            if (float.IsInfinity(response.fBrake) || float.IsNaN(response.fBrake))
                response.fBrake = 0.0f;

            if (float.IsInfinity(response.fSteering) || float.IsNaN(response.fSteering))
                response.fSteering = 0.0f;

            // Store for next update
            mLastOutputMap[_player] = response;
        }


        public ISimulationOutput ProduceSimulationOutput()
        {
            UpdateTurn();

            SimulationOutput toReturn = new SimulationOutput();
            toReturn.players = mPlayerMap.Values.ToArray();
            toReturn.obstacles = mObstacles;
            toReturn.projectiles = mProjectiles.ToArray();
            toReturn.track = mTrack;
            toReturn.totalLaps = mTrack.TotalLaps;
            toReturn.totalTime = mTotalTime;
            toReturn.uiCurrentTurn = (UInt32)muiCurrentTurn;
            toReturn.uiGridWidth = muiGridWidth;
            toReturn.uiGridHeight = muiGridHeight;
            toReturn.turnTime = mFrequency;

            return toReturn;
        }

        public void ApplyRendererFeedback(IRendererFeedback _rendererFeedback)
        {
            RendererFeedback feedback = _rendererFeedback as RendererFeedback;
            if (feedback == null)
                return;

            // Just check if the renderer wants us to quit
            if (feedback.ShouldQuit)
            {
                mIsGameFinished = true;
                mbShouldRestart = feedback.ShouldRestart;
            }
        }

        public void OnGameEnd()
        {
            stopwatch.Stop();
        }

        public bool IsGameFinished()
        {
            return mIsGameFinished;
        }

        public bool ShouldRestart()
        {
            return mbShouldRestart;
        }

        public TimeSpan GetFrequency()
        {
            return mFrequency;
        }
        #endregion


        #region Private Interface

        TrackObject[] mObstacles = null;
        List<Projectile> mProjectiles = new List<Projectile>();
        Track mTrack = new Track();

        bool mIsGameFinished = false;
        bool mbShouldRestart = false;
        Random mRNG = new Random();

        // Parameters
        const string mParametersFile = "Parameters.xml";
        UInt32 muiCurrentTurn = 0;
        UInt32 muiGridWidth = 1024;
        UInt32 muiGridHeight = 768;
        UInt32 muiTrackID = 0;

        TimeSpan mTotalTime = new TimeSpan(0);
        TimeSpan mFrequency = new TimeSpan(0, 0, 0, 0, 33);	// 33 ms per frame

        // Game data
        Dictionary<IPlayer, SimulationOutput.PlayerData> mPlayerMap = new Dictionary<IPlayer, SimulationOutput.PlayerData>();
        Dictionary<IPlayer, CarData> mCarDataMap = new Dictionary<IPlayer, CarData>();
        Dictionary<IPlayer, PlayerOutput> mLastOutputMap = new Dictionary<IPlayer, PlayerOutput>();

        void Init()
        {
            mPlayerMap.Clear();
            mCarDataMap.Clear();
            mLastOutputMap.Clear();

            mIsGameFinished = false;
            mbShouldRestart = false;

            InitParameters();
            InitTrack();
            InitRace();

            // Generate obstacles
            ObstacleGenerator generator = new ObstacleGenerator(mRNG, (Int32)muiGridWidth, (Int32)muiGridHeight);
            mObstacles = generator.GetTrackRandomObstacles(mTrack);
        }

        void InitParameters()
        {
            if (!File.Exists(mParametersFile))
                return;

            try
            {
                XElement root = XElement.Load(mParametersFile);

                XElement gridWidthNode = root.Descendants("GridWidth").FirstOrDefault();
                UInt32.TryParse(gridWidthNode.Value, out muiGridWidth);

                XElement gridHeightNode = root.Descendants("GridHeight").FirstOrDefault();
                UInt32.TryParse(gridHeightNode.Value, out muiGridHeight);

                XElement trackIDNode = root.Descendants("TrackID").FirstOrDefault();
                UInt32.TryParse(trackIDNode.Value, out muiTrackID);

                int msFreq = 0;
                XElement timeSpanNode = root.Descendants("TimeSpan").FirstOrDefault();
                if (int.TryParse(timeSpanNode.Value, out msFreq))
                {
                    mFrequency = new TimeSpan(0, 0, 0, 0, msFreq);
                    fDeltaT = (float)msFreq / 1000.0f;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Failed to load XML param file: " + e.Message);
            }
        }

        void InitTrack()
        {
            switch(muiTrackID)
            {
                default:
                case 1:
                    mTrack.InitTrack1(muiGridWidth, muiGridHeight);
                    break;
                case 2:
                    mTrack.InitTrack2(muiGridWidth, muiGridHeight);
                    break;
                case 3:
                    mTrack.InitTrack3(muiGridWidth, muiGridHeight);
                    break;
                case 4:
                    mTrack.InitTrack4(muiGridWidth, muiGridHeight);
                    break;
                case 5:
                    mTrack.InitTrack5(muiGridWidth, muiGridHeight);
                    break;
            }
        }


        public void Shuffle<T>(IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = mRNG.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        void InitRace()
        {
            int iNumPlayers = PlayerList.Count();
            float fRandomHue = (float)mRNG.NextDouble();
            float fHueIncrement = (iNumPlayers > 0) ? 1.0f / (float)iNumPlayers : 1.0f;

            string strCurDirectory = Directory.GetCurrentDirectory();
            bool bSamePos = File.Exists(Path.Combine(strCurDirectory, "SamePos.txt"));
            int iSamePosY = mRNG.Next(0, (Int32)muiGridHeight);

            foreach (IPlayer curPlayer in PlayerList)
            {
                SimulationOutput.PlayerData newPlayerData = new SimulationOutput.PlayerData();
                CarData newCarData = new CarData();

                newPlayerData.player = curPlayer;
                newPlayerData.color = GetColorFromHue(fRandomHue);
                fRandomHue = (fRandomHue + fHueIncrement) % 1.0f;

				Vector2 pos = mTrack.CenterLine.Eval(mTrack.StartLine);
				Vector2 tan = mTrack.CenterLine.EvalTangent(mTrack.StartLine);
				Vector2 defaultDir = new Vector2(1.0f, 0.0f);

				newPlayerData.curPos = pos;
				newPlayerData.fHeading = Utils.DirectionScreenCoordsToHeading(tan);

				newCarData.carPosition = newPlayerData.curPos;
                newCarData.oldPosition = newCarData.carPosition;
				newCarData.boundingBox = new Box(newCarData.carPosition, CarData.carSize, Utils.HeadingToScreenAngle(newPlayerData.fHeading));

                mPlayerMap.Add(curPlayer, newPlayerData);
                mCarDataMap.Add(curPlayer, newCarData);
                mLastOutputMap.Add(curPlayer, new PlayerOutput());
            }
        }

        Color GetColorFromHue(float _fHue)
        {
            float fR = 0.0f;
            float fG = 0.0f;
            float fB = 0.0f;

            const float fOneThird = 1.0f / 3.0f;
            const float fTwoThirds = 2.0f / 3.0f;

            if (_fHue < fOneThird)
            {
                float fLerp = _fHue / fOneThird;
                if (fLerp < 0.5f)
                {
                    fLerp *= 2.0f;
                    fR = 1.0f;
                    fG = Math.Max(fG, fLerp);
                }
                else
                {
                    fLerp = (fLerp - 0.5f) * 2.0f;
                    fG = 1.0f;
                    fR = Math.Max(fR, 1.0f - fLerp);
                }

            }
            else if (_fHue < fTwoThirds)
            {
                _fHue -= fOneThird;
                float fLerp = _fHue / fOneThird;
                if (fLerp < 0.5f)
                {
                    fLerp *= 2.0f;
                    fG = 1.0f;
                    fB = Math.Max(fB, fLerp);
                }
                else
                {
                    fLerp = (fLerp - 0.5f) * 2.0f;
                    fB = 1.0f;
                    fG = Math.Max(fG, 1.0f - fLerp);
                }
            }
            else
            {
                _fHue -= fTwoThirds;
                float fLerp = _fHue / fOneThird;
                if (fLerp < 0.5f)
                {
                    fLerp *= 2.0f;
                    fB = 1.0f;
                    fR = Math.Max(fR, fLerp);
                }
                else
                {
                    fLerp = (fLerp - 0.5f) * 2.0f;
                    fR = 1.0f;
                    fB = Math.Max(fB, 1.0f - fLerp);
                }
            }


            return Color.FromArgb(255, (byte)(fR * 255.0f), (byte)(fG * 255.0f), (byte)(fB * 255.0f));
        }

        void UpdateTurn()
        {
            foreach (SimulationOutput.PlayerData curPlayerData in mPlayerMap.Values)
            {
                PlayerOutput lastOutput = mLastOutputMap[curPlayerData.player];
                CarData carData = mCarDataMap[curPlayerData.player];
                UpdatePlayerCar(lastOutput, carData, curPlayerData);

                if (lastOutput.bShotProjectile && curPlayerData.uiAmmo > 0)
                {
                    --curPlayerData.uiAmmo;
                    Projectile projectile = new Projectile(carData.carPosition.X, carData.carPosition.Y, 8, 8);
                    projectile.trackProjectile.lastUsageMap = new Dictionary<Color, float>();
                    projectile.trackProjectile.objectType = TrackObjectType.Projectile;
                    projectile.trackProjectile.lastUsageMap[curPlayerData.color] = 1.0f;
                    lastOutput.aimDirection.Normalize();
                    if (lastOutput.aimDirection.LengthSquared() < 0.9f)
                    {
                        lastOutput.aimDirection = carData.carDirection;
                        lastOutput.aimDirection.Normalize();
                    }

                    projectile.vVelocity = lastOutput.aimDirection * fProjectileSpeed;
                    mProjectiles.Add(projectile);
                }
            }

            ++muiCurrentTurn;

            // Check if a player has reached the finish line
            bool allCarFinished = true;
            foreach (SimulationOutput.PlayerData curData in mPlayerMap.Values)
            {
                if (curData.lap < mTrack.TotalLaps)
                {
                    allCarFinished = false;
                    break;
                }
            }

            mTotalTime = stopwatch.Elapsed;

            if (allCarFinished)
            {
                mIsGameFinished = true;
            }
        }

        void UpdatePlayerCar(PlayerOutput _lastOutput, CarData _playerCar, SimulationOutput.PlayerData _playerData)
        {
            PlayerOutput lastOutput = _lastOutput;
            CarData carData = _playerCar;

			carData.carDirection = Utils.HeadingToDirectionScreenCoords(_playerData.fHeading);

            //skip updating simulation if player is stun
            if (UpdatePlayerStun(carData))
                lastOutput = new PlayerOutput();

            //straight line car simulation computation
            Vector2 forceTraction = carData.carDirection;

            //check for obstacle
            bool isOnGrass = mTrack.IsOnGrass(_playerCar.carPosition);
            bool hitSomething = false;
            TrackObjectType obstacleType = TrackObjectType.None;

			//update bounding box
			carData.boundingBox.Center = carData.carPosition;
			carData.boundingBox.Orientation = Utils.HeadingToScreenAngle(_playerData.fHeading);

			if (!mTrack.IsOnNavMesh(carData.boundingBox))
			{
				hitSomething = true;
				obstacleType = TrackObjectType.Wall;
			}

			foreach (TrackObject curObstacle in mObstacles)
            {
                if (!hitSomething && UpdateTrackObject(curObstacle, _playerData, carData.boundingBox))
                {
                    hitSomething = true;
                    obstacleType = curObstacle.objectType;
                }
            }

            UpdateProjectiles(_playerCar, _playerData, carData.boundingBox);
            
            // Apply throttle
            float availableForce = CarData.fEngineForce * (isOnGrass ? 0.4f : 1.0f);
            forceTraction *= availableForce * lastOutput.fThrottle;

            //air resistance
            Vector2 forceDrag = carData.carVelocity;

            //magic number, drag air resistant constant for a Corvette on earth
            const float fAirResistance = -0.4257f;

            //forceDrag.Length == speed
            forceDrag *= forceDrag.Length();
            forceDrag *= fAirResistance;
			//rolling resistance

			Vector2 forceRollResistance = carData.carVelocity;

            //Rolling resistance constant is 30 time higher then air resistance constant
            forceRollResistance *= fAirResistance * 30;

            Vector2 forceLongtitudinal = new Vector2();
            forceLongtitudinal += forceTraction;
            forceLongtitudinal += forceDrag;
            forceLongtitudinal += forceRollResistance;

            //divide by mass give us acceleration
            forceLongtitudinal /= CarData.fCarMass;

            
            forceLongtitudinal *= fDeltaT;

            //adjust velocity for brake and collision
            bool goingBackward = false;
            if (lastOutput.fBrake > lastOutput.fThrottle)
            {
                if (forceLongtitudinal.Length() > carData.carVelocity.Length())
                {
                    //no reverse when braking ! Clamp to 0
                    //remove this to allow going backward using brake
                    forceLongtitudinal.X = carData.carVelocity.X * -1.0f;
                    forceLongtitudinal.Y = carData.carVelocity.Y * -1.0f;
                }
            }

            carData.carVelocity += forceLongtitudinal;

            // Apply brakes
            float fBrakes = Math.Max(1.0f - lastOutput.fBrake * CarData.fBrakeScale * fDeltaT, 0.0f);
            carData.carVelocity *= fBrakes;

            // Clamp position on edges
            if (carData.carPosition.X < 0.0f || carData.carPosition.X > muiGridWidth
                || carData.carPosition.Y < 0.0f || carData.carPosition.Y > muiGridHeight)
                hitSomething = true;

            if (hitSomething)
            {
                switch(obstacleType)
                {
                    case TrackObjectType.Wall:
                        float colTime = mTrack.CenterLine.GetClosestTimeToPoint(carData.oldPosition);
                        Vector2 colCenter = mTrack.CenterLine.Eval(colTime);
                        Vector2 colTan = mTrack.CenterLine.EvalTangent(colTime);

                        // Normal pointing toward inside
                        Vector2 colNormalLeft = Vector2.PerpendicularCounterClockwise(colTan);
                        Vector2 colNormalRight = Vector2.PerpendicularClockwise(colTan);
						
						// Check if car was going in same direction as track
						bool wasForward = true;
                        if (carData.carVelocity.LengthSquared() > 0.01f && Vector2.Dot(carData.carDirection, colTan) < 0.0f)
                            wasForward = false;

                        // Set velocity in the track tangent direction and preserve speed, this will force the car to go in track direction
						Vector2 finalTan = (wasForward ? colTan : -colTan);
						carData.carVelocity = Vector2.Project(carData.carVelocity, finalTan);

						// Get speed at collision time
						float colSpeed = carData.carVelocity.Length();

						carData.carVelocity.Normalize();

						// If speed was higher than certain speed, slowly reduce his speed by 50 units/second
						const float wallHitMaxSpeed = 20.0f;
                        const float wallHitSlowSpeed = 50.0f;
                        if (colSpeed > wallHitMaxSpeed)
                        {
                            colSpeed -= fDeltaT * wallHitSlowSpeed;
                            colSpeed = Math.Max(wallHitMaxSpeed, colSpeed);
                        }

                        carData.carVelocity *= colSpeed;

                        // Find which wall is closer to car (normal pointing inside so negate it)
                        float distLeft = ((colCenter - colNormalLeft) - carData.carPosition).LengthSquared();
                        float distRight = ((colCenter - colNormalRight) - carData.carPosition).LengthSquared();

                        // Push car back toward the inside of the track (1 unit)
                        const float pushInsideStrength = 1.0f;
                        if (distLeft < distRight)
                            carData.carPosition += colNormalLeft * pushInsideStrength;
                        else
                            carData.carPosition += colNormalRight * pushInsideStrength;

                        break;
					case TrackObjectType.Bounce:
                        // Move player out of obstacle
                        carData.carPosition = carData.oldPosition;

                        //hitting something mean receiving a greater opposite force (in this game at least)
                        //this will force the player backward
                        float fSpeed = carData.carVelocity.Length();
                        if (fSpeed > 0.0f)
                        {
                            carData.carVelocity /= fSpeed;
                            carData.carVelocity *= -50.0f;
                        }
                        break;
                    case TrackObjectType.Boost:
                        carData.carVelocity *= fBoosterVelocityMultiplier;
                        break;
                    case TrackObjectType.PowerUp:
                        _playerData.uiAmmo++;
                        break;
                    default:
                        break;
                }
            }

            //handle reverse

            if (carData.carVelocity.LengthSquared() > 0.01f &&
                Vector2.Dot(carData.carDirection, carData.carVelocity) < 0.0f)
                goingBackward = true;


            Vector2 velocityOnPlayer = carData.carVelocity;
            velocityOnPlayer *= fDeltaT;
            //player pos += velocityOnPlayer

            float angularTurnSpeedInRadPerSec = (float)Utils.Pi;  //full turn in 2 sec
            if (isOnGrass)
                angularTurnSpeedInRadPerSec *= 0.5f;
            float maxTurnPerFrame = angularTurnSpeedInRadPerSec * fDeltaT;

            //now calculate steering for this player
            //every good side project need a complicated use of ternary operator (clamp value)
            float fSteeringRad = Utils.Clamp(lastOutput.fSteering, -1.0f, 1.0f);

            // progressive steering
            float steerDelta = fSteeringRad - carData.fLastSteering;
            if (Math.Abs(steerDelta) > maxTurnPerFrame)
                fSteeringRad = carData.fLastSteering + maxTurnPerFrame * Math.Sign(steerDelta);

            //fSteeringRad *= angularTurnSpeedInRadPerSec * fDeltaT;
            float fTurningCircleRadius = (float)(CarData.fCarLength / fSteeringRad);



            carData.fLastSteering = fSteeringRad;

            float fAngularVelocity = carData.carVelocity.Length() / fTurningCircleRadius; // in rad per sec

            carData.carVelocity.Rotate(fAngularVelocity * fDeltaT);

            //velocity in meter/s
            float fVelLen = carData.carVelocity.Length();

            if (fVelLen > 0.0f)
            {
                carData.carDirection = carData.carVelocity;
                carData.carDirection /= fVelLen;

                if (goingBackward)
                    carData.carDirection *= -1.0f;
            }

			float fHeading = Utils.DirectionScreenCoordsToHeading(carData.carDirection);

			// speed * deltaT for displacement
			carData.oldPosition = carData.carPosition;
            carData.carPosition.X += carData.carVelocity.X * fDeltaT;
            carData.carPosition.Y += carData.carVelocity.Y * fDeltaT;
			carData.uiAmmo = _playerData.uiAmmo;
			carData.boundingBox.Center = carData.carPosition;
			carData.boundingBox.Orientation = Utils.HeadingToScreenAngle(fHeading);

			// Feed infos for renderer
            // keep current heading when hitting wall

            UpdateLap(carData, mTrack, _playerData);

            _playerData.fHeading = fHeading;
            _playerData.curPos = carData.carPosition;

            _playerData.fBrake = _lastOutput.fBrake;
            _playerData.fThrottle = _lastOutput.fThrottle;
            _playerData.fSteering = fSteeringRad;
            _playerData.debugLines = _lastOutput.DebugLines;
            _playerData.turnTime = _lastOutput.TurnTime;
			_playerData.boundingBox = carData.boundingBox;

			if (_playerData.finishTime.Ticks == 0 && _playerData.lap >= mTrack.TotalLaps)
            {
                _playerData.finishTime = stopwatch.Elapsed;
            }
        }

		void UpdateLap(CarData carData, Track track, SimulationOutput.PlayerData playerData)
		{
			float time = track.CenterLine.GetClosestTimeToPoint(carData.carPosition);
			int currentSegment = track.CenterLine.GetSegmentFromTime(time);
			bool found = false;
			for (int i = 0; i < carData.lapSegments.Count; i++)
			{
				if (carData.lapSegments[i] == currentSegment)
				{
					found = true;
					break;
				}
			}

			if (!found && carData.currentLap == playerData.lap)
			{
				carData.lapSegments.Add(currentSegment);
			}

			LapResult lapResult = IsFinishLineCrossed(carData, track);
			if (lapResult == LapResult.Forward && 
				carData.lapSegments.Count == track.CenterLine.GetSegmentCount() &&
				playerData.lap == carData.currentLap)
			{
				playerData.lap++;
				carData.currentLap = Math.Max(carData.currentLap, playerData.lap);

				carData.lapSegments.Clear();
			}
		}

		enum LapResult
		{
			None,
			Forward,
			Backward,
		}

		LapResult IsFinishLineCrossed(CarData carData, Track track)
        {
			Vector2 oldPos = carData.oldPosition;
			Vector2 newPos = carData.carPosition;

            Vector2 p0 = track.CenterLine.Eval(track.StartLine);
            Vector2 t0 = track.CenterLine.EvalTangent(track.StartLine);

            float halfWidth = track.GrassWidth * 0.5f;

            Vector2 p00 = Vector2.PerpendicularClockwise(t0);
            p00 = p0 + (p00 * halfWidth);

            Vector2 p01 = Vector2.PerpendicularCounterClockwise(t0);
            p01 = p0 + (p01 * halfWidth);

            if ( Line.Intersect(p00, p01, oldPos, newPos) )
            {
                Vector2 dir = newPos - oldPos;
                dir.Normalize();

                if (Vector2.Dot(t0, dir) >= 0)
				{
					return LapResult.Forward;
				}
				else
				{
					return LapResult.Backward;
				}
			}

            return LapResult.None;
        }

        bool UpdatePlayerStun(CarData _playerCar)
        {
            if (!_playerCar.stunTimer.IsRunning)
                return false;//not stun

            if (_playerCar.stunTimer.Elapsed.TotalSeconds > 2.0f)
            {
                _playerCar.stunTimer.Reset();
            }
            else
            {
                _playerCar.carVelocity *= 0.0f;
                _playerCar.carDirection.Rotate(Utils.Pi * 2.0f * fDeltaT);
            }

            return _playerCar.stunTimer.IsRunning;
        }

        void UpdateProjectiles(CarData _playerCar, SimulationOutput.PlayerData _playerData, Box playerBox)
        {
            List<Projectile> projectileToDetonate = new List<Projectile>();
            foreach (Projectile projectile in mProjectiles)
            {
                if (UpdateTrackObject(projectile.trackProjectile, _playerData, playerBox))
                {
                    _playerCar.stunTimer.Restart();
                    projectileToDetonate.Add(projectile);
                }
                else
                {
					Vector2 posUpdate = projectile.trackProjectile.Box.Center;
					posUpdate.X += (float)Math.Round(projectile.vVelocity.X * fDeltaT);
					posUpdate.Y += (float)Math.Round(projectile.vVelocity.Y * fDeltaT);

					projectile.trackProjectile.Box.Center = posUpdate;
					if (!mTrack.IsOnNavMesh(projectile.trackProjectile.Box))
					{
						projectileToDetonate.Add(projectile);
					}
                    else // Destroy projectile if they hit another projectile or obstacle
                    {
                        bool destroyed = false;
                        
                        //Check bounce obstacles
                        foreach(TrackObject obj in mObstacles)
                        {
                            if ((obj.objectType == TrackObjectType.Bounce) &&
                                (obj.Box.Intersect(projectile.trackProjectile.Box)))
                            {
                                projectileToDetonate.Add(projectile);
                                destroyed = true;
                                break;
                            }
                        }

                        //Check other projectiles
                        if (!destroyed)
                        {
                            foreach (Projectile otherProjectile in mProjectiles)
                            {
                                if (projectile != otherProjectile)
                                {
                                    if (projectile.trackProjectile.Box.Intersect(otherProjectile.trackProjectile.Box))
                                    {
                                        projectileToDetonate.Add(projectile);
                                        destroyed = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            mProjectiles = mProjectiles.Except(projectileToDetonate).ToList();       
        }

        bool UpdateTrackObject(TrackObject curTrackObj, SimulationOutput.PlayerData _playerData, Box playerBox)
        {
            if (curTrackObj.lastUsageMap.ContainsKey(_playerData.color))
            {
                if (curTrackObj.lastUsageMap[_playerData.color] > fDeltaT)
                {
                    curTrackObj.lastUsageMap[_playerData.color] -= fDeltaT;
                    return false;
                }
                else
                {
                    curTrackObj.lastUsageMap.Remove(_playerData.color);
                }
            }

			if (curTrackObj.Box.Intersect(playerBox))
			{
				curTrackObj.lastUsageMap[_playerData.color] = fObjCooldownInSec;
				return true;
			}

			return false;
        }

        #endregion
    }

    class ObstacleGenerator
    {
        class Generator
        {
            public float Probability { get; set; }

            public delegate TrackObject[] GenerateObstaclesFn();
            public GenerateObstaclesFn GenerateObstacles;
        }

        Random mRNG = null;
        List<Generator> mGeneratorList = new List<Generator>();
        float mfTotalProbabilities = 0.0f;
        int miWidth = 0;
        int miHeight = 0;

        public ObstacleGenerator(Random _parentRNG, int _iW, int _iH)
        {
            mRNG = _parentRNG;
            miWidth = _iW;
            miHeight = _iH;

            AddGenerator(new Generator { Probability = 0.05f, GenerateObstacles = StaticGeneration_1 });
            AddGenerator(new Generator { Probability = 0.05f, GenerateObstacles = StaticGeneration_2 });
            AddGenerator(new Generator { Probability = 0.50f, GenerateObstacles = RandomGeneration_Uniform });
            AddGenerator(new Generator { Probability = 0.30f, GenerateObstacles = RandomGeneration_Edges_H });
            AddGenerator(new Generator { Probability = 0.25f, GenerateObstacles = RandomGeneration_Edges_V });

        }

        private void AddGenerator(Generator _toAdd)
        {
            mGeneratorList.Add(_toAdd);
            mfTotalProbabilities += _toAdd.Probability;
        }

        public TrackObject[] GetRandomObstacles()
        {
            float fDomain = (float)mRNG.NextDouble() * mfTotalProbabilities;

            float fCurX = 0.0f;
            Generator toUse = null;
            foreach (Generator curGen in mGeneratorList)
            {
                fCurX += curGen.Probability;
                if (fCurX > fDomain)
                {
                    toUse = curGen;
                    break;
                }
            }

            TrackObject[] toReturn = toUse.GenerateObstacles();
            return toReturn;
        }

        public TrackObject[] GetTrackRandomObstacles(Track track)
        {
            uint minObs = track.ObstacleMin;
            uint maxObs = track.ObstacleMax;
            uint minPwr = track.PowerupMin;
            uint maxPwr = track.PowerupMax;
            uint minBoost = track.BoosterMin;
            uint maxBoost = track.BoosterMax;

            int nbObstacles = mRNG.Next((int)minObs, (int)maxObs + 1);
            int nbPowerup = mRNG.Next((int)minPwr, (int)maxPwr + 1);
            int nbBooster = mRNG.Next((int)minBoost, (int)maxBoost + 1);
            int nbObjects = nbObstacles + nbPowerup + nbBooster;

            float objectSpan = 1.0f / (float)nbObjects;

            TrackObject[] toReturn = new TrackObject[nbObjects];
            float roadHalfWidth = track.RoadWidth * 0.5f;

            for (int i=0; i< nbObjects; i++)
            {
                float location = (float)(mRNG.NextDouble() * objectSpan) + (i * objectSpan);

                Vector2 objectPos = track.CenterLine.Eval(location);
                Vector2 objectTan = track.CenterLine.EvalTangent(location);
                Vector2 objectPerp = Vector2.PerpendicularCounterClockwise(objectTan);

                TrackObject obj = null;
                int objectType = 0;
                bool objectPicked = false;
                do
                {
                    objectType = mRNG.Next(0, 3);
                    if (objectType == 0 && nbPowerup > 0)
                    {
                        nbPowerup--;
                        objectPicked = true;
                    }
                    else if (objectType == 1 && nbObstacles > 0)
                    {
                        nbObstacles--;
                        objectPicked = true;
                    }
                    else if (objectType == 2 && nbBooster > 0)
                    {
                        nbBooster--;
                        objectPicked = true;
                    }
                } while (!objectPicked);

                float test = Utils.DirectionScreenCoordsToHeading(objectTan);
                Vector2 test2 = Utils.HeadingToDirectionScreenCoords(test);

                if (objectType == 0)
                {
                    float width = 8.0f;
                    float height = 8.0f;

                    float objectOffset = (float)((mRNG.NextDouble() * 2.0 - 1.0) * (roadHalfWidth - width / 2));
                    Vector2 objectFinalPos = objectPos + objectPerp * objectOffset;
                    float x = objectFinalPos.X;
                    float y = objectFinalPos.Y;

                    float heading = Utils.DirectionScreenCoordsToHeading(objectTan);
                    float orientation = Utils.HeadingToScreenAngle(heading);

                    obj = new TrackObject(new Vector2(x, y), new Vector2(width, height), orientation);
                    obj.objectType = TrackObjectType.PowerUp;
                    obj.lastUsageMap = new Dictionary<Color, float>();
                }
                else if (objectType == 1)
                {
                    float width = (float)(mRNG.NextDouble() * 0.5 + 0.5) * (roadHalfWidth * 0.5f);
                    float height = (float)(mRNG.NextDouble() * 0.5 + 0.5) * (roadHalfWidth * 0.5f);

                    float objectOffset = (float)((mRNG.NextDouble() * 2.0 - 1.0) * (roadHalfWidth - width / 2));
                    Vector2 objectFinalPos = objectPos + objectPerp * objectOffset;
                    float x = objectFinalPos.X;
                    float y = objectFinalPos.Y;

                    float heading = Utils.DirectionScreenCoordsToHeading(objectTan);
                    float orientation = Utils.HeadingToScreenAngle(heading);

                    obj = new TrackObject(new Vector2(x, y), new Vector2(width, height), orientation);
                    obj.objectType = TrackObjectType.Bounce;
                    obj.lastUsageMap = new Dictionary<Color, float>();
                }
                else if (objectType == 2)
                {
                    float width = 8.0f;
                    float height = 8.0f;

                    float objectOffset = (float)((mRNG.NextDouble() * 2.0 - 1.0) * (roadHalfWidth - width / 2));
                    Vector2 objectFinalPos = objectPos + objectPerp * objectOffset;
                    float x = objectFinalPos.X;
                    float y = objectFinalPos.Y;

                    float heading = Utils.DirectionScreenCoordsToHeading(objectTan);
                    float orientation = Utils.HeadingToScreenAngle(heading);

                    obj = new TrackObject(new Vector2(x, y), new Vector2(width, height), orientation);
                    obj.objectType = TrackObjectType.Boost;
                    obj.lastUsageMap = new Dictionary<Color, float>();
                }

                toReturn[i] = obj;
            }

            return toReturn;
        }

        private TrackObject[] StaticGeneration_1()
        {
            TrackObject[] obstacles = new TrackObject[]
            {
                new TrackObject ( 100,  10,  10,  70 ),
                new TrackObject ( 140, 200,  90,  20 ),
                new TrackObject ( 240,   5, 190,  25 ),
                new TrackObject ( 300,  80,  15,  50 ),
                new TrackObject ( 350, 125,   5,  45 ),
                new TrackObject ( 420, 200,  85,  10 ),
                new TrackObject ( 510,  15,  15,  75 ),
                new TrackObject ( 620,  25,  25, 120 ),
                new TrackObject ( 800, 100,  20, 100 ),
            };

            return obstacles;
        }

        private TrackObject[] StaticGeneration_2()
        {
            TrackObject[] obstacles = new TrackObject[]
            {
                new TrackObject ( 50,  30, 100, 10 ),
                new TrackObject ( 50, 200,  75,  5 ),
                new TrackObject ( 50, 120,  50, 20 ),
                new TrackObject (190,  10, 125, 15 ),
                new TrackObject (170,  90,  85, 10 ),
                new TrackObject (210, 170, 140, 15 ),
                new TrackObject (290,  70, 200, 10 ),
                new TrackObject (290,  70, 200, 10 ),
                new TrackObject (350, 220, 150, 25 ),
                new TrackObject (500,  35, 125, 12 ),
                new TrackObject (520, 150,  95,  7 ),
                new TrackObject (590, 215, 195,  9 ),
                new TrackObject (630, 105, 135, 13 ),
                new TrackObject (750,  61, 185, 17 ),
                new TrackObject (799, 161,  99, 11 ),
            };

            return obstacles;
        }

        private TrackObject[] RandomGeneration_Uniform()
        {
            int iInitialBuffer = 50;

            int iObstacleSize = mRNG.Next(15) + 5;  // [5, 20]
            int iObstacleArea = iObstacleSize * iObstacleSize;
            float fObstacleAmount = (float)mRNG.NextDouble() * 15.0f + 15.0f;

            int iGridSize = (miWidth - (iObstacleSize * 2)) * (miHeight - (iObstacleSize * 2));
            int iObstacleCount = (int)((float)iGridSize / ((float)iObstacleArea * fObstacleAmount));

            List<TrackObject> list = new List<TrackObject>();
            for (int iCurObstacle = 0; iCurObstacle < iObstacleCount; ++iCurObstacle)
            {
                int iX = mRNG.Next(miWidth - iObstacleSize - iInitialBuffer) + iInitialBuffer;
                int iY = mRNG.Next(miHeight - iObstacleSize);
                list.Add(new TrackObject ( iX, iY, iObstacleSize, iObstacleSize ));
            }

            return list.ToArray();
        }

        private TrackObject[] RandomGeneration_Edges_H()
        {
            int iInitialBuffer = 50;

            float fObstacleAmount = (float)mRNG.NextDouble() * (float)miWidth + (float)(miWidth * 1.6f);

            int iGridSize = miWidth * miHeight;
            int iObstacleCount = (int)((float)iGridSize / fObstacleAmount);

            List<TrackObject> list = new List<TrackObject>();
            for (int iCurObstacle = 0; iCurObstacle < iObstacleCount; ++iCurObstacle)
            {
                int iObstacleWidth = mRNG.Next(40) + 20;    // [20, 60]
                int iObstacleHeight = iObstacleWidth / 8;

                int iX = mRNG.Next(miWidth - iObstacleWidth * 2 - iInitialBuffer) + iObstacleWidth + iInitialBuffer;

                // Obstacles should more likely appear on edges of the map
                double dY = mRNG.NextDouble() * 2.0 - 1.0;
                dY = Math.Pow(Math.Abs(dY), 0.8) * Math.Sign(dY);

                int iY = (int)((dY * 0.5 + 0.5) * (miHeight - iObstacleHeight));
                list.Add(new TrackObject ( iX, iY, iObstacleWidth, iObstacleHeight ));
            }

            return list.ToArray();
        }

        private TrackObject[] RandomGeneration_Edges_V()
        {
            float fObstacleAmount = (float)(mRNG.NextDouble() * (float)miWidth + (float)miWidth) * 1.6f + 15.0f;

            int iGridSize = miWidth * miHeight;
            int iObstacleCount = (int)((float)iGridSize / fObstacleAmount);

            int iBufferLeft = miWidth / 10;

            List<TrackObject> list = new List<TrackObject>();
            for (int iCurObstacle = 0; iCurObstacle < iObstacleCount; ++iCurObstacle)
            {
                int iObstacleHeight = mRNG.Next(45) + 20;   // [20, 65]
                int iObstacleWidth = iObstacleHeight / 10;

                int iX = mRNG.Next(miWidth - 2 * iObstacleWidth) + iObstacleWidth + iBufferLeft;

                // Obstacles should more likely appear on edges of the map
                double dY = mRNG.NextDouble() * 2.0 - 1.0;
                dY = Math.Pow(Math.Abs(dY), 0.6) * Math.Sign(dY);

                int iY = (int)((dY * 0.5 + 0.5) * (miHeight - iObstacleHeight));
                list.Add(new TrackObject (iX, iY, iObstacleWidth, iObstacleHeight ));
            }

            return list.ToArray();
        }
    }
}
