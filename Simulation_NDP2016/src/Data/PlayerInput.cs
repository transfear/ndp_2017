﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;
using Maths;

namespace Simulation_NDP2016.Data
{
	public class PlayerInput : IPlayerInput
	{
        /// <summary> Struct representing data related to a player </summary>
		public class PlayerData
		{
            /// <summary> Unique player index </summary>
			public UInt32 uiPlayerIndex;

            /// <summary>
            /// Your car position in the world
            /// </summary>
            public Vector2 vPosition = new Vector2();

            /// <summary>
            /// What direction your car is currently facing
            /// </summary>
            public Vector2 vDirection = new Vector2();

            /// <summary>
            /// Current car velocity
            /// </summary>
            public Vector2 vVelocity = new Vector2();

			/// <summary>
			/// Car oriented bounding box
			/// </summary>
			public Box boundingBox = new Box();

			/// <summary>
			/// Current amount of ammo
			/// </summary>
			public uint uiAmmo;

            public PlayerData CopyDeep()
            {
                PlayerData copy = new PlayerData();
                copy.uiPlayerIndex = uiPlayerIndex;
                copy.vPosition = vPosition;
                copy.vDirection = vDirection;
                copy.vVelocity = vVelocity;
				copy.uiAmmo = uiAmmo;
				copy.boundingBox = boundingBox.Clone() as Box;

                return copy;
            }
        };

		/// <summary>
		/// Time delta since last update
		/// </summary>
		public TimeSpan deltaT;

		/// <summary>
		/// Contains current player data
		/// </summary>
		public PlayerData playerData;

		/// <summary>
		/// Contains the list of track objects (boosters and obstacles)
		/// </summary>
		public TrackObject[] objects;

		/// <summary>
		/// Contains the list of projectiles
		/// </summary>
		public Projectile[] projectiles;

		/// <summary>
		/// The track data itself
		/// </summary>
		public Track track;

		/// <summary>
		/// Contains the list of all other players
		/// </summary>
		public PlayerData[] otherPlayerData;

		/// <summary>
		/// Booster velocity multiplier, passing on a booster will multiply your velocity by this amount
		/// </summary>
		public float boosterVelocityMultiplier;

		/// <summary>
		/// Velocity of a thrown projectile 
		/// </summary>
		public float projectileSpeed;

		// From IPlayerInputInterface
		public IPlayerInput CopyDeep()
        {
            PlayerInput copy = new PlayerInput();

            // Copy player data
            copy.playerData = playerData.CopyDeep();

            // Copy objects
            Int32 iNumObstacles = objects.GetLength(0);
            copy.objects = new TrackObject[iNumObstacles];
            Array.Copy(objects, copy.objects, iNumObstacles);

			// Copy projectiles
			Int32 iNumProjectile = projectiles.GetLength(0);
            copy.projectiles = new Projectile[iNumProjectile];
            Array.Copy(projectiles, copy.projectiles, iNumProjectile);

			// Copy other players
			int iNumPlayers = otherPlayerData.GetLength(0);
			copy.otherPlayerData = new PlayerData[iNumPlayers];
			for (int i = 0; i < iNumPlayers; i++)
				copy.otherPlayerData[i] = otherPlayerData[i].CopyDeep();

			// Copy track
			if (track != null)
                copy.track = (Track)track.Clone();

			// Copy time delta
			copy.deltaT = deltaT;

			copy.boosterVelocityMultiplier = boosterVelocityMultiplier;
			copy.projectileSpeed = projectileSpeed;

			return copy;
        }
    }
}
