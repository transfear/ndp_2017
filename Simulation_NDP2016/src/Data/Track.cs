﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Maths;
using System.Xml.Linq;
using System.IO;

namespace Simulation_NDP2016.Data
{
    [Serializable]
    public class Track : ICloneable
    {
        /// <summary>
		/// Total width of grass (this can also be thought as the full width of the track before walls)
		/// </summary>
		public float GrassWidth;
		/// <summary>
		/// Total width of road
		/// </summary>
		public float RoadWidth;
		/// <summary>
		/// Start line time position on spline
		/// </summary>
		public float StartLine;
		/// <summary>
		/// Amount of steps to split map for drawing it
		/// </summary>
		public int DrawSteps;

        private uint mWidth;
        private uint mHeight;

        private UInt32 _id = 0;
        /// <summary>
		/// Track ID
		/// </summary>
		public UInt32 ID { get { return _id; } }

        private UInt32 _totalLaps = 1;
        /// <summary>
		/// Number of laps required to finish the track
		/// </summary>
		public UInt32 TotalLaps { get { return _totalLaps; } }

        private UInt32 _obstacleMin = 0;
        /// <summary>
		/// Generation minimum count of obstacles
		/// </summary>
		public UInt32 ObstacleMin { get { return _obstacleMin; } }

        private UInt32 _obstacleMax = 0;
		/// <summary>
		/// Generation maximum count of obstacles
		/// </summary>
		public UInt32 ObstacleMax { get { return _obstacleMax; } }

        private UInt32 _powerupMin = 0;
		/// <summary>
		/// Generation minimum count of powerup
		/// </summary>
		public UInt32 PowerupMin { get { return _powerupMin; } }

        private UInt32 _powerupMax = 0;
		/// <summary>
		/// Generation maximum count of powerup
		/// </summary>
		public UInt32 PowerupMax { get { return _powerupMax; } }

        private UInt32 _boosterMin = 0;
		/// <summary>
		/// Generation minimum count of boosters
		/// </summary>
		public UInt32 BoosterMin { get { return _boosterMin; } }

        private UInt32 _boosterMax = 0;
		/// <summary>
		/// Generation maximum count of boosters
		/// </summary>
		public UInt32 BoosterMax { get { return _boosterMax; } }

        /// <summary>
		/// The spline defining the center of the track
		/// </summary>
		public Spline CenterLine;

        public object Clone()
        {
            return Utils.DeepClone<Track>(this);
        }

        private List<Triangle> BuildNavMesh()
        {
            if (QuadTree.Instance != null)
            {
                QuadTree.ShutdownInstance();
            }

            List<Triangle> mesh = new List<Triangle>(); ;
            mesh.Clear();

            float halfWidth = GrassWidth * 0.5f;
            int nbSteps = 2048;

            float step = 1.0f / (float)nbSteps;

            bool side = false;
            float time = 0.0f;
            int count = 0;

            Vector2 p0 = Vector2.Zero, p1 = Vector2.Zero, p2 = Vector2.Zero, t0;

            //Based on tris
            while (time <= (1.0f + step))
            {
                t0 = CenterLine.EvalTangent(time);
                p2 = CenterLine.Eval(time);

                Vector2 p00;
                if (side)
                    p00 = Vector2.PerpendicularClockwise(t0);
                else
                    p00 = Vector2.PerpendicularCounterClockwise(t0);

                p2 = p2 + (p00 * halfWidth);
                side = !side;

                count++;
                if (count >= 3)
                {
                    mesh.Add(new Triangle(p0, p1, p2));
                }

                p0 = p1;
                p1 = p2;
                time += step;
            }

            return mesh;
        }

        private void BuildQuadTree(List<Triangle> mesh)
        {
            QuadTree.ShutdownInstance();
            QuadTree.InitializeInstance(new AxisAlignedBox(0.0f, 0.0f, mWidth, mHeight), 7);
            QuadTree.Instance.AddTriangles(mesh);
            QuadTree.Instance.PruneEmptyNodes(QuadTree.Instance);
        }

        /// <summary>
		/// Allows to know if a given position in located on the navigation mesh (the track)
		/// </summary>
		/// <param name="position">Position to test with</param>
		/// <returns>True if point is on navigation mesh, false otherwise</returns>
		public bool IsOnNavMesh(Vector2 position)
        {
            List<Triangle> tris = QuadTree.Instance.GetTrianglesFromPoint(position);
            if (tris != null)
            {
                for (int i = 0; i < tris.Count; i++)
                {
                    Triangle tri = tris[i];
                    if (tri.Intersect(position))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

		/// <summary>
		/// Allows to know if a given box in fully located on the navigation mesh (the track)
		/// </summary>
		/// <param name="box">Box to test with</param>
		/// <returns>True if box is on navigation mesh, false otherwise</returns>
		public bool IsOnNavMesh(Box box)
        {
            return IsOnNavMesh(box.Corners[0]) && IsOnNavMesh(box.Corners[1]) && IsOnNavMesh(box.Corners[2]) && IsOnNavMesh(box.Corners[3]);
        }

		/// <summary>
		/// Allows to know if a given point is located on grass
		/// </summary>
		/// <param name="position">Position to test with</param>
		/// <returns>True if point is on grass, false otherwise</returns>
		public bool IsOnGrass(Vector2 position)
        {
            float time = CenterLine.GetClosestTimeToPoint(position);
            Vector2 center = CenterLine.Eval(time);

            float distFromCenter = (position - center).Length();
            return distFromCenter > (RoadWidth * 0.5f);
        }

        private bool RaycastTrack(Line line, out Vector2 intersectPos)
        {
            Vector2 dir = line.P1 - line.P0;
            float lengthSq = dir.LengthSquared();

            dir.Normalize();

            Vector2 start = line.P0;
            Vector2 end = line.P1;
            Vector2 intersect = new Vector2(float.PositiveInfinity, float.PositiveInfinity);

            Vector2 pt = line.P0;
            bool cont = true;
            Triangle lastTri = null;
            float jump = 1.0f;
            do
            {
                bool bFound = false;

                List<Triangle> tris = QuadTree.Instance.GetTrianglesFromPoint(pt);
                if (tris != null)
                {
                    for (int i = 0; i < tris.Count; i++)
                    {
                        if ((pt - start).LengthSquared() >= lengthSq)
                        {
                            bFound = false;
                            intersect = new Vector2(float.PositiveInfinity, float.PositiveInfinity);
                            break;
                        }

                        Triangle tri = tris[i];
                        if (tri.Intersect(pt))
                        {
                            if (lastTri == tri)
                            {
                                jump += 1.0f;
                            }

                            lastTri = tri;
                            Vector2 testIntersect;
                            if (Line.Intersect(pt, end, tri.P0, tri.P1, out testIntersect) ||
                                Line.Intersect(pt, end, tri.P1, tri.P2, out testIntersect) ||
                                Line.Intersect(pt, end, tri.P2, tri.P0, out testIntersect))
                            {
                                intersect = testIntersect;
                                pt = testIntersect + (dir * jump);
                            }
                            else
                            {
                                pt = pt + (dir * jump);
                            }

                            bFound = true;
                            break;
                        }
                    }
                }

                if (!bFound)
                    cont = false;

            } while (cont);

            intersectPos = intersect;
            if (float.IsPositiveInfinity(intersectPos.X) && float.IsPositiveInfinity(intersectPos.Y))
                return false;

            return true;
        }

        private bool RaycastTrack(Vector2 start, Vector2 dir, out Vector2 intersectPos)
        {
            float length = new Vector2((float)mWidth, (float)mHeight).Length();
            dir.Normalize();

            Vector2 end = start + (length * dir);

            return RaycastTrack(new Line(start, end), out intersectPos);
        }

        /// <summary>
		/// Raycast in object list and track to find collision point.
		/// </summary>
		/// <param name="objMask">The mask of objects types to test against</param>
		/// <param name="objects">The objects array to test with</param>
		/// <param name="line">The line defining the ray for intersection test</param>
		/// <param name="intersectPos">Returns the intersection position</param>
		/// <returns>Return the closest track object hit by the raycast, TrackObjectType.None if no hit occurred.</returns>
		public TrackObjectType Raycast(TrackObjectType objMask, TrackObject[] objects, Line line, out Vector2 intersectPos)
        {
            float distance = float.MaxValue;
            TrackObjectType objType = TrackObjectType.None;
            intersectPos = new Vector2(float.PositiveInfinity, float.PositiveInfinity);

            Vector2 objIntersectPt;
            if (objects != null)
            {
                foreach (TrackObject trackObject in objects)
                {
                    if ((trackObject.objectType & objMask) != 0 &&
                        trackObject.Raycast(line, out objIntersectPt))
                    {
                        float localDistance = (objIntersectPt - line.P0).LengthSquared();
                        if (localDistance < distance)
                        {
                            distance = localDistance;
                            intersectPos = objIntersectPt;
                            objType = trackObject.objectType;
                        }
                    }
                }
            }

            if ((TrackObjectType.Wall & objMask) != 0 &&
                RaycastTrack(line, out objIntersectPt))
            {
                float localDistance = (objIntersectPt - line.P0).LengthSquared();
                if (localDistance < distance)
                {
                    distance = localDistance;
                    intersectPos = objIntersectPt;
                    objType = TrackObjectType.Wall;
                }
            }

            return objType;
        }

		/// <summary>
		/// Raycast in object list and track to find collision point.
		/// </summary>
		/// <param name="objMask">The mask of objects types to test against</param>
		/// <param name="objects">The objects array to test with</param>
		/// <param name="start">The starting position of the ray</param>
		/// <param name="dir">The direction of the ray</param>
		/// <param name="intersectPos">Returns the intersection position</param>
		/// <returns>Return the closest track object hit by the raycast, TrackObjectType.None if no hit occurred.</returns>
		public TrackObjectType Raycast(TrackObjectType objMask, TrackObject[] objects, Vector2 start, Vector2 dir, out Vector2 intersectPos)
        {
            float length = new Vector2((float)mWidth, (float)mHeight).Length();
            dir.Normalize();
            Vector2 end = start + (length * dir);
            Line ray = new Line(start, end);

            return Raycast(objMask, objects, ray, out intersectPos);
        }

		/// <summary>
		/// Raycast in all object list/types and track to find collision point.
		/// </summary>
		/// <param name="objects">The objects array to test with</param>
		/// <param name="start">The starting position of the ray</param>
		/// <param name="dir">The direction of the ray</param>
		/// <param name="intersectPos">Returns the intersection position</param>
		/// <returns>Return the closest track object hit by the raycast, TrackObjectType.None if no hit occurred.</returns>
		public TrackObjectType Raycast(TrackObject[] objects, Vector2 start, Vector2 dir, out Vector2 intersectPos)
        {
            return Raycast(TrackObjectType.All, objects, start, dir, out intersectPos);
        }

		/// <summary>
		/// Raycast in all object list/types and track to find collision point.
		/// </summary>
		/// <param name="objects">The objects array to test with</param>
		/// <param name="line">The line defining the ray for intersection test</param>
		/// <param name="intersectPos">Returns the intersection position</param>
		/// <returns>Return the closest track object hit by the raycast, TrackObjectType.None if no hit occurred.</returns>
		public TrackObjectType Raycast(TrackObject[] objects, Line line, out Vector2 intersectPos)
        {
            return Raycast(TrackObjectType.All, objects, line, out intersectPos);
        }

		/// <summary>
		/// Raycast the track to find collision point.
		/// </summary>
		/// <param name="line">The line defining the ray for intersection test</param>
		/// <param name="intersectPos">Returns the intersection position</param>
		/// <returns>Return TrackObjectType.Wall if hit by the raycast, TrackObjectType.None if no hit occurred.</returns>
		public TrackObjectType Raycast(Line line, out Vector2 intersectPos)
        {
            return Raycast(TrackObjectType.All, null, line, out intersectPos);
        }

		/// <summary>
		/// Raycast the track to find collision point.
		/// </summary>
		/// <param name="start">The starting position of the ray</param>
		/// <param name="dir">The direction of the ray</param>
		/// <param name="intersectPos">Returns the intersection position</param>
		/// <returns>Return TrackObjectType.Wall if hit by the raycast, TrackObjectType.None if no hit occurred.</returns>
		public TrackObjectType Raycast(Vector2 start, Vector2 dir, out Vector2 intersectPos)
        {
            return Raycast(TrackObjectType.All, null, start, dir, out intersectPos);
        }

        private void SetupTrack()
        {
            List<Triangle> mesh = BuildNavMesh();
            BuildQuadTree(mesh);
        }

        void InitParameters(int trackID)
        {
            string trackFile = "Track" + trackID.ToString() + ".xml";

            if (!File.Exists(trackFile))
                return;

            try
            {
                XElement root = XElement.Load(trackFile);

                XElement trackIDNode = root.Descendants("TrackID").FirstOrDefault();
                UInt32.TryParse(trackIDNode.Value, out _id);

                XElement totalLapsNode = root.Descendants("TotalLaps").FirstOrDefault();
                UInt32.TryParse(totalLapsNode.Value, out _totalLaps);

                XElement trackObstacleMinNode = root.Descendants("ObstacleMin").FirstOrDefault();
                UInt32.TryParse(trackObstacleMinNode.Value, out _obstacleMin);

                XElement trackObstacleMaxNode = root.Descendants("ObstacleMax").FirstOrDefault();
                UInt32.TryParse(trackObstacleMaxNode.Value, out _obstacleMax);

                XElement trackPowerupMinNode = root.Descendants("PowerupMin").FirstOrDefault();
                UInt32.TryParse(trackPowerupMinNode.Value, out _powerupMin);

                XElement trackPowerupMaxNode = root.Descendants("PowerupMax").FirstOrDefault();
                UInt32.TryParse(trackPowerupMaxNode.Value, out _powerupMax);

                XElement trackBoosterMinNode = root.Descendants("BoosterMin").FirstOrDefault();
                UInt32.TryParse(trackBoosterMinNode.Value, out _boosterMin);

                XElement trackBoosterMaxNode = root.Descendants("BoosterMax").FirstOrDefault();
                UInt32.TryParse(trackBoosterMaxNode.Value, out _boosterMax);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Failed to load XML track file: " + e.Message);
            }
        }

        public void InitTrack1(uint width, uint height)
        {
            mWidth = width;
            mHeight = height;
            GrassWidth = 100.0f;
            RoadWidth = 60.0f;
            DrawSteps = 320;
            StartLine = 0.06f;

            InitParameters(1);

            float Border = 70.0f;
            float CornerEntry = Math.Min(width, height) / 10.0f;

            Vector2[] points =
            {
                new Vector2( Border + CornerEntry, Border ),
                new Vector2( width - Border - CornerEntry, Border ),
                new Vector2( width - Border, Border + CornerEntry ),
                new Vector2( width - Border, height - Border - CornerEntry ),
                new Vector2( width - Border - CornerEntry, height - Border ),
                new Vector2( Border + CornerEntry, height - Border ),
                new Vector2( Border, height - Border - CornerEntry ),
                new Vector2( Border, Border + CornerEntry ),
                new Vector2( Border + CornerEntry, Border ),
            };

            float tanScale = 100.0f;
            Vector2[] tans =
            {
                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 0.0f, 1.0f * tanScale ),
                new Vector2( 0.0f, 1.0f * tanScale ),
                new Vector2( -1.0f * tanScale, 0.0f ),
                new Vector2( -1.0f * tanScale, 0.0f ),
                new Vector2( 0.0f, -1.0f * tanScale ),
                new Vector2( 0.0f, -1.0f * tanScale ),
                new Vector2( 1.0f * tanScale, 0.0f ),
            };

            CenterLine = new Spline(points, tans);

            SetupTrack();
        }

		public void InitTrack2(uint width, uint height)
		{
			mWidth = width;
			mHeight = height;

			GrassWidth = 100.0f;
			RoadWidth = 60.0f;
			DrawSteps = 320;
			StartLine = 0.10f;

			InitParameters(2);

			float Border = 70.0f;
			float CornerEntry = Math.Min(width, height) / 10.0f;

			Vector2 Center = new Vector2(width / 2.0f, height / 2.0f);

			Vector2[] points =
			{
				new Vector2( Border + CornerEntry, Border ),
				new Vector2( Border + 2.0f * CornerEntry, Border ),
				new Vector2( Center.X, Center.Y ),

				new Vector2( width - (Border + 2.0f * CornerEntry), height - Border ),
				new Vector2( width - (Border + CornerEntry), height - Border ),
				new Vector2( width - Border, height - Border - CornerEntry ),

				new Vector2( width - Border, Border + CornerEntry ),
				new Vector2( width - (Border + CornerEntry), Border ),
				new Vector2( width - (Border + 2.0f * CornerEntry), Border ),

				new Vector2( Center.X, Center.Y ),
				new Vector2( Border + 2.0f * CornerEntry, height - Border ),
				new Vector2( Border + CornerEntry, height - Border ),

				new Vector2( Border, height - (Border + CornerEntry) ),
				new Vector2( Border, Border + CornerEntry ),
				new Vector2( Border + CornerEntry, Border ),
			};

			float tanScale = 50.0f;
			Vector2[] tans =
			{
				new Vector2( 1.0f * tanScale, 0.0f ),
				new Vector2( 1.0f * tanScale, 1.0f * tanScale ),
				new Vector2( 1.0f * tanScale, 1.0f * tanScale ),

				new Vector2( 1.0f * tanScale, 0.0f ),
				new Vector2( 1.0f * tanScale, -1.0f * tanScale ),
				new Vector2( 0.0f, -1.0f * tanScale ),

				new Vector2( -1.0f * tanScale, -1.0f * tanScale ),
				new Vector2( -1.0f * tanScale, 0.0f ),
				new Vector2( -1.0f * tanScale, 1.0f * tanScale ),

				new Vector2( -1.0f * tanScale, 1.0f * tanScale ),
				new Vector2( -1.0f * tanScale, 0.0f ),
				new Vector2( -1.0f * tanScale, -1.0f * tanScale ),

				new Vector2( 0.0f, -1.0f * tanScale ),
				new Vector2( 1.0f * tanScale, -1.0f * tanScale ),
				new Vector2( 1.0f * tanScale, 0.0f ),
			};

			CenterLine = new Spline(points, tans);

			SetupTrack();
		}

		public void InitTrack3(uint width, uint height)
        {
            mWidth = width;
            mHeight = height;

            GrassWidth = 100.0f;
            RoadWidth = 60.0f;
            DrawSteps = 320;
            StartLine = 0.75f;

            InitParameters(3);

            float Border = 70.0f;
            float CornerEntry = Math.Min(width, height) / 10.0f;

            Vector2 CenterLeft = new Vector2(width / 2.0f - CornerEntry, height / 2.0f);
            Vector2 CenterRight = new Vector2(width / 2.0f + CornerEntry, height / 2.0f);

            Vector2[] points =
            {
                new Vector2( Border + CornerEntry, Border ),
                new Vector2( CenterLeft.X - CornerEntry, Border + CornerEntry ),
                new Vector2( CenterLeft.X - CornerEntry, Border + 2.5f * CornerEntry ),
                CenterLeft,
                CenterRight,
                new Vector2( CenterRight.X + CornerEntry, Border + 2.5f * CornerEntry ),
                new Vector2( CenterRight.X + CornerEntry, Border + CornerEntry ),
                new Vector2( width - Border - CornerEntry, Border ),
                new Vector2( width - Border, Border + CornerEntry ),
                new Vector2( width - Border, height - Border - CornerEntry ),
                new Vector2( width - Border - CornerEntry, height - Border ),
                new Vector2( Border + CornerEntry, height - Border ),
                new Vector2( Border, height - Border - CornerEntry ),
                new Vector2( Border, Border + CornerEntry ),
                new Vector2( Border + CornerEntry, Border ),
            };

            float tanScale = 50.0f;
            float tanScaleDbl = tanScale * 2.0f;
            float tanScaleHalf = tanScale * 0.5f;
            Vector2[] tans =
            {
                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 1.0f * tanScaleDbl, 1.0f * tanScale ),
                new Vector2( 0.0f * tanScale, 1.0f * tanScale ),
                new Vector2( 1.0f * tanScale, 0.0f * tanScale ),
                new Vector2( 1.0f * tanScale, 0.0f * tanScale ),
                new Vector2( 0.0f * tanScale, -1.0f * tanScale ),
                new Vector2( 1.0f * tanScaleDbl, -1.0f * tanScale ),
                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 0.0f, 1.0f * tanScale ),
                new Vector2( 0.0f, 1.0f * tanScale ),
                new Vector2( -1.0f * tanScale, 0.0f ),
                new Vector2( -1.0f * tanScale, 0.0f ),
                new Vector2( 0.0f, -1.0f * tanScale ),
                new Vector2( 0.0f, -1.0f * tanScale ),
                new Vector2( 1.0f * tanScale, 0.0f ),
            };

            CenterLine = new Spline(points, tans);

            SetupTrack();
        }

		public void InitTrack4(uint width, uint height)
        {
            mWidth = width;
            mHeight = height;
            
            GrassWidth = 100.0f;
            RoadWidth = 60.0f;
            DrawSteps = 800;
            StartLine = 0.95f;

            InitParameters(4);

            float TightCornerWidth = 100.0f;
            float TightCornerHeight = 30.0f;
            float TopBorder = 150.0f;
            float BotBorder = 200.0f;
            float Border = 70.0f;
            float CornerEntry = Math.Min(width, height) / 10.0f;

            Vector2[] points =
            {
                new Vector2( Border + CornerEntry, TopBorder ),

                new Vector2( Border + CornerEntry + (1.0f * TightCornerWidth), TopBorder - TightCornerHeight ),
                new Vector2( Border + CornerEntry + (2.0f * TightCornerWidth), TopBorder + TightCornerHeight ),
                new Vector2( Border + CornerEntry + (3.0f * TightCornerWidth), TopBorder - TightCornerHeight ),
                new Vector2( Border + CornerEntry + (4.0f * TightCornerWidth), TopBorder + TightCornerHeight ),
                new Vector2( Border + CornerEntry + (5.0f * TightCornerWidth), TopBorder - TightCornerHeight ),
                new Vector2( Border + CornerEntry + (6.0f * TightCornerWidth), TopBorder + TightCornerHeight ),
                new Vector2( Border + CornerEntry + (7.0f * TightCornerWidth), TopBorder - TightCornerHeight ),
                new Vector2( Border + CornerEntry + (8.0f * TightCornerWidth), TopBorder + TightCornerHeight ),
                new Vector2( Border + CornerEntry + (9.0f * TightCornerWidth), TopBorder - TightCornerHeight ),
                new Vector2( Border + CornerEntry + (10.0f * TightCornerWidth), TopBorder + TightCornerHeight ),
                new Vector2( Border + CornerEntry + (11.0f * TightCornerWidth), TopBorder - TightCornerHeight ),

                new Vector2( width - Border - CornerEntry, TopBorder ),
                new Vector2( width - Border, TopBorder + CornerEntry ),
                new Vector2( width - Border - 2.0f * CornerEntry, height / 2.0f ),
                new Vector2( width - Border, height - BotBorder - CornerEntry ),
                new Vector2( width - Border - CornerEntry, height - BotBorder ),

                new Vector2( width - Border - CornerEntry - (1.0f * TightCornerWidth), height - BotBorder - 0.6f * TightCornerHeight ),
                new Vector2( width - Border - CornerEntry - (2.0f * TightCornerWidth), height - BotBorder + 0.8f * TightCornerHeight ),
                new Vector2( width - Border - CornerEntry - (3.0f * TightCornerWidth), height - BotBorder - 1.05f * TightCornerHeight ),
                new Vector2( width - Border - CornerEntry - (4.0f * TightCornerWidth), height - BotBorder + 1.35f * TightCornerHeight ),
                new Vector2( width - Border - CornerEntry - (5.0f * TightCornerWidth), height - BotBorder - 1.7f * TightCornerHeight ),
                new Vector2( width - Border - CornerEntry - (6.0f * TightCornerWidth), height - BotBorder + 2.1f * TightCornerHeight ),
                new Vector2( width - Border - CornerEntry - (7.0f * TightCornerWidth), height - BotBorder - 2.55f * TightCornerHeight ),
                new Vector2( width - Border - CornerEntry - (8.0f * TightCornerWidth), height - BotBorder + 3.05f * TightCornerHeight ),
                new Vector2( width - Border - CornerEntry - (9.0f * TightCornerWidth), height - BotBorder - 3.6f * TightCornerHeight ),
                new Vector2( width - Border - CornerEntry - (10.0f * TightCornerWidth), height - BotBorder + 3.7f * TightCornerHeight ),

                new Vector2( Border + CornerEntry, height - BotBorder ),
                new Vector2( Border, height - BotBorder - CornerEntry ),
                new Vector2( Border + CornerEntry, height / 2.0f ),
                new Vector2( Border, TopBorder + CornerEntry ),
                new Vector2( Border + CornerEntry, TopBorder ),
            };

            float tanScale = 150.0f;
            Vector2[] tans =
            {
                new Vector2( 1.0f * tanScale, 0.0f ),

                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 1.0f * tanScale, 0.0f ),

                new Vector2( 1.0f * tanScale, 0.0f ),
                new Vector2( 0.0f, 1.0f * tanScale ),
                new Vector2( 0.0f, 1.0f * tanScale ),
                new Vector2( 0.0f, 1.0f * tanScale ),
                new Vector2( -1.0f * tanScale, 0.0f ),

                new Vector2( -1.0f * tanScale, -1.0f * tanScale ),
                new Vector2( -1.0f * tanScale, +1.0f * tanScale ),
                new Vector2( -1.0f * tanScale, -1.0f * tanScale ),
                new Vector2( -1.0f * tanScale, +1.0f * tanScale ),
                new Vector2( -1.0f * tanScale, -1.0f * tanScale ),
                new Vector2( -1.0f * tanScale, +1.0f * tanScale ),
                new Vector2( -1.0f * tanScale, -1.0f * tanScale ),
                new Vector2( -1.0f * tanScale, +1.0f * tanScale ),
                new Vector2( -1.0f * tanScale, -1.0f * tanScale ),
                new Vector2( -1.0f * tanScale, +1.0f * tanScale ),

                new Vector2( -1.0f * tanScale, 0.0f ),
                new Vector2( 0.0f, -1.0f * tanScale ),
                new Vector2( 0.0f, -1.0f * tanScale ),
                new Vector2( 0.0f, -1.0f * tanScale ),
                new Vector2( 1.0f * tanScale, 0.0f ),
            };

            CenterLine = new Spline(points, tans);

            SetupTrack();
        }

        public void InitTrack5(uint width, uint height)
        {
            mWidth = width;
            mHeight = height;
            
            GrassWidth = 100.0f;
            RoadWidth = 60.0f;
            DrawSteps = 440;
            StartLine = 0.065f;

            InitParameters(5);

            float Border = 70.0f;
            float CornerEntry = Math.Min(width, height) / 10.0f;
            Vector2 Center = new Vector2(width / 2.0f, height / 2.0f);

            Vector2[] points =
            {
                new Vector2( Border + CornerEntry, Border ),
                new Vector2( Border + 2.0f * CornerEntry, Border ),
                new Vector2( Border + 4.0f * CornerEntry, Border + CornerEntry ),
                new Vector2( Border + 4.0f * CornerEntry, height - Border - CornerEntry ),

                new Vector2( Center.X, height - Border ),

                new Vector2( width - Border - 4.0f * CornerEntry, height - Border - CornerEntry ),
                new Vector2( width - Border - 4.0f * CornerEntry, Border + CornerEntry ),
                new Vector2( width - Border - 2.0f * CornerEntry, Border ),
                new Vector2( width - Border - CornerEntry, Border ),

                new Vector2( width - Border, Border + CornerEntry ),
                new Vector2( width - Border, Center.Y - CornerEntry ),

                new Vector2( width - Border - CornerEntry, Center.Y ),
                new Vector2( Center.X, Center.Y ),
                new Vector2( Border + CornerEntry, Center.Y ),

                new Vector2( Border, Center.Y - CornerEntry ),
                new Vector2( Border, Border + CornerEntry ),

                new Vector2( Border + CornerEntry, Border ),
            };

            float tanScale = 100.0f;
            Vector2[] tans =
            {
                new Vector2( tanScale, 0.0f ),
                new Vector2( 4.0f * tanScale, 0.0f ),
                new Vector2( 0.0f, tanScale ),
                new Vector2( tanScale, 2.0f * tanScale ),

                new Vector2( 2.0f * tanScale, 0.0f ),

                new Vector2( tanScale, -2.0f * tanScale ),
                new Vector2( 0.0f, -tanScale ),
                new Vector2( 4.0f * tanScale, 0.0f ),
                new Vector2( tanScale, 0.0f ),

                new Vector2( 0.0f, tanScale ),
                new Vector2( 0.0f, tanScale ),

                new Vector2( -tanScale, 0.0f ),
                new Vector2( -tanScale, 0.0f ),
                new Vector2( -tanScale, 0.0f ),

                new Vector2( 0.0f, -tanScale ),
                new Vector2( 0.0f, -tanScale ),

                new Vector2( tanScale, 0.0f ),
            };

            CenterLine = new Spline(points, tans);

            SetupTrack();
        }
    }
}
