﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Maths;
using System.Drawing;

namespace Simulation_NDP2016.Data
{
    [Flags]
    public enum TrackObjectType
    {
        /// <summary>
		/// No object
		/// </summary>
		None = 0x0,

        /// <summary>
		/// Object is a wall
		/// </summary>
		Wall = 0x1,
        /// <summary>
		/// Object is a boost
		/// </summary>
		Boost = 0x2,
		/// <summary>
		/// Object is a bounce pad
		/// </summary>
		Bounce = 0x4,
		/// <summary>
		/// Object is a power up
		/// </summary>
		PowerUp = 0x8,
		/// <summary>
		/// Object is a projectile
		/// </summary>
		Projectile = 0x10,

		/// <summary>
		/// Mask for all objects
		/// </summary>
		All = Wall | Boost | Bounce | PowerUp | Projectile,
    };

    /// <summary>
	/// Represents an object on the track
	/// </summary>
	public class TrackObject
    {
		public TrackObject(Vector2 center, Vector2 size, float orientation)
		{
			Init(center, size, orientation);
		}

		public TrackObject(float x, float y, float width, float height, float orientation)
		{
			Init(new Vector2(x, y), new Vector2(width, height), orientation);
		}

		public TrackObject(float x, float y, float width, float height)
		{
			Init(new Vector2(x, y), new Vector2(width, height), 0.0f);
		}

		public void Init(Vector2 center, Vector2 size, float orientation)
		{
			Box = new Box(center, size, orientation);
		}

		/// <summary>
		/// The bounding box of the object
		/// </summary>
		public Box Box = null;

        /// <summary>
		/// Object type
		/// </summary>
		public TrackObjectType objectType;

        public Dictionary<Color, float> lastUsageMap;

		/// <summary>
		/// Intersection test between the box and a point (box encloses point)
		/// </summary>
		/// <param name="vPos">The point to test with</param>
		/// <returns>Returns true if there was intersection, false otherwise</returns>
		public bool Intersect(Vector2 vPos)
        {
			return Box.Intersect(vPos);
        }

		/// <summary>
		/// Intersection test against the box and a line
		/// </summary>
		/// <param name="line">Line to test the box with</param>
		/// <returns>Returns true if there was intersection, false otherwise</returns>
		public bool Intersect(Line line)
        {
            return Box.Intersect(line);
        }

        /// <summary>
		/// Raycast against the box and a line
		/// </summary>
		/// <param name="line">Line defining the ray</param>
		/// <param name="intersectPt">Returns the intersection point on the box</param>
		/// <returns>Returns true if there was intersection, false otherwise</returns>
		public bool Raycast(Line line, out Vector2 intersectPt)
        {
            return Box.Raycast(line, out intersectPt);
        }

        public bool IsAvailable(Color fPlayerColor)
        {
            return lastUsageMap.ContainsKey(fPlayerColor);        
        }
    
    }

	/// <summary>
	/// Represents a projectile
	/// </summary>
	public class Projectile
    {
		private Projectile()
		{
		}

		public Projectile(float x, float y, float width, float height)
		{
			trackProjectile = new TrackObject(x, y, width, height);
		}

        /// <summary>
		/// The track object representing the projectile, contains position, bounding box, etc.
		/// </summary>
		public TrackObject trackProjectile;
        
		/// <summary>
		/// Velocity of the projectile
		/// </summary>
		public Vector2 vVelocity;
    }
}
