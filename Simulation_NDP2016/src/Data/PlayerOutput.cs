﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;
using Maths;

namespace Simulation_NDP2016.Data
{
    /// <summary>
    /// </summary>
    public class PlayerOutput : IPlayerOutput
    {
		/// <summary>
		/// Throttle applied to car. Between 0.0f and 1.0f. 0.0f == no throttle, 1.0f == full throttle
		/// </summary>
		public float fThrottle;

		/// <summary>
		/// Brake applied to car. Between 0.0f and 1.0f. 0.0f == no brake, 1.0f == full brakes
		/// </summary>
		public float fBrake;

		/// <summary>
		/// Steering wheel turn angle applied to car in radian. Between -1.0f and 1.0f. -1.0f == full left, 1.0f == full right
		/// </summary>
		public float fSteering;

		/// <summary>
		/// Shooting direction vector of shot projectile.
		/// </summary>
		public Vector2 aimDirection;

		/// <summary>
		/// Player wants to shoot a projectile, set to true to fire it.
		/// </summary>
		public bool bShotProjectile;

		/// <summary>
		/// Drawable debug lines, provide a list of lines in order to draw them. Do not forget to activate the option in UI
		/// </summary>
		public List<Line> DebugLines = new List<Line>();

        // From IPlayerOutput interface
        public override IPlayerOutput CopyDeep()
        {
            PlayerOutput copy = new PlayerOutput();
            copy.fThrottle = fThrottle;
            copy.fBrake = fBrake;
            copy.fSteering = fSteering;
            copy.aimDirection = aimDirection;
            copy.bShotProjectile = bShotProjectile;
            copy.DebugLines = DebugLines.Select(item => (Line)item.Clone()).ToList();
            copy.TurnTime = TurnTime;

            return copy;
        }
    }
}
