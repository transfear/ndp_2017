﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;
using FrameworkInterface.Simulation;
using Maths;

namespace Simulation_NDP2016.Data
{
    public class SimulationOutput : ISimulationOutput
    {
        public class PlayerData
        {
            public IPlayer  player;
            public Color color;
            public Vector2 curPos = new Vector2();
            public float fHeading = 0.0f;           // Between 0 and 2PI. 0 == right, PI/2 == up, PI == left, 3PI/2 == bottom

            public float fThrottle;     // Between  0.0f and 1.0f.  0.0f == no throttle, 1.0f == full throttle
            public float fBrake;        // Between  0.0f and 1.0f.  0.0f == no brake, 1.0f == full brakes
            public float fSteering;     // Between -1.0f and 1.0f. -1.0f == full left, 1.0f == full right. Represent wheel angle in radia

            public uint uiAmmo;

			public Box boundingBox;

            public int lap = 0;
            public TimeSpan finishTime = new TimeSpan(0);
            public TimeSpan turnTime = new TimeSpan(0);

            public List<Line> debugLines;

            public PlayerData CopyDeep()
            {
                PlayerData copy = new PlayerData();
                copy.player = player;
                copy.color = color;
                copy.curPos = curPos;
                copy.fHeading = fHeading;

                copy.fThrottle = fThrottle;
                copy.fBrake = fBrake;
                copy.fSteering = fSteering;

                copy.lap = lap;
                copy.finishTime = finishTime;
                copy.turnTime = turnTime;

                copy.uiAmmo = uiAmmo;

				copy.boundingBox = boundingBox.Clone() as Box;

                copy.debugLines = debugLines.Select(item => (Line)item.Clone()).ToList();

                return copy;
            }
        };

		public PlayerData[] players;
        public TrackObject[] obstacles;
        public Projectile[] projectiles;
        public Track track;
        public UInt32 totalLaps = 1;

        public UInt32 uiCurrentTurn = 0;
        public TimeSpan turnTime;
        public UInt32 uiGridWidth = 0;
        public UInt32 uiGridHeight = 0;

        public TimeSpan totalTime;

        // From ISimulationOutput interface
        public ISimulationOutput CopyDeep()
		{
            SimulationOutput copy = new SimulationOutput();

            // Copy players
            Int32 iNumPlayers = players.GetLength(0);
            copy.players = new PlayerData[iNumPlayers];
            for (Int32 iCurPlayer = 0; iCurPlayer < iNumPlayers; ++iCurPlayer)
                copy.players[iCurPlayer] = players[iCurPlayer].CopyDeep();

            // Copy obstacles
            Int32 iNumObstacles = obstacles.GetLength(0);
            copy.obstacles = new TrackObject[iNumObstacles];
            Array.Copy(obstacles, copy.obstacles, iNumObstacles);
            
            // Copy projectiles
            Int32 iNumProjectiles = projectiles.GetLength(0);
            copy.projectiles = new Projectile[iNumProjectiles];
            Array.Copy(projectiles, copy.projectiles, iNumProjectiles);

            // Copy track
            copy.track = track.Clone() as Track;

            copy.uiCurrentTurn = uiCurrentTurn;
            copy.uiGridWidth = uiGridWidth;
            copy.uiGridHeight = uiGridHeight;
            copy.totalLaps = totalLaps;
            copy.totalTime = totalTime;
            copy.turnTime = turnTime;

            return copy;
		}
	}
}
