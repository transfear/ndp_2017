﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;
using FrameworkInterface.Renderer;

namespace FrameworkInterface.Simulation
{
	public abstract class ISimulationInstantiator
	{
		// Responsible for simulation instantiation
		public abstract ISimulation InstantiateSimulation(List<Type> _simulationTypeList);
		
		// Responsible for renderer instantiation
		public abstract IRenderer InstantiateRenderer(List<Type> _rendererTypeList);

		// Responsible for player instantiation
		public abstract List<IPlayer> InstantiatePlayers(List<Type> _playerTypeList);
	}
}
