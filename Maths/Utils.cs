﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Maths
{
    public static class Utils
    {
        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }

        /// <summary>
		/// Small epsilon for floating point tolerance
		/// </summary>
		public const float ZeroTolerance = 1e-6f;
        /// <summary>
		/// PI constant
		/// </summary>
		public const float Pi = (float)Math.PI;
        /// <summary>
		/// 2 * PI constant
		/// </summary>
		public const float TwoPi = (float)(2 * Math.PI);
        /// <summary>
		/// PI / 2 constant
		/// </summary>
		public const float PiOverTwo = (float)(Math.PI / 2);
        /// <summary>
		/// PI / 4 constant
		/// </summary>
		public const float PiOverFour = (float)(Math.PI / 4);

        /// <summary>
		/// Compare two floats applying ZeroTolerance epsilon
		/// </summary>
		/// <param name="a">First number to test</param>
		/// <param name="b">Second number to test</param>
		/// <returns>Returns true if both number are near equal, false otherwise</returns>
		public static bool NearEqual(float a, float b)
        {
            //C heck if the numbers are really close -- needed
            // when comparing numbers near zero.
            if (IsZero(a - b))
                return true;

            return WithinEpsilon(a, b, ZeroTolerance);
        }

        public static bool IsZero(float a)
        {
            return Math.Abs(a) < ZeroTolerance;
        }

        public static bool IsOne(float a)
        {
            return IsZero(a - 1.0f);
        }

        public static bool WithinEpsilon(float a, float b, float epsilon)
        {
            float num = a - b;
            return ((-epsilon <= num) && (num <= epsilon));
        }

        public static float RevolutionsToDegrees(float revolution)
        {
            return revolution * 360.0f;
        }

        public static float RevolutionsToRadians(float revolution)
        {
            return revolution * TwoPi;
        }

        public static float RevolutionsToGradians(float revolution)
        {
            return revolution * 400.0f;
        }

        public static float DegreesToRevolutions(float degree)
        {
            return degree / 360.0f;
        }

        public static float DegreesToRadians(float degree)
        {
            return degree * (Pi / 180.0f);
        }

        public static float RadiansToRevolutions(float radian)
        {
            return radian / TwoPi;
        }

        public static float RadiansToGradians(float radian)
        {
            return radian * (200.0f / Pi);
        }

        public static float GradiansToRevolutions(float gradian)
        {
            return gradian / 400.0f;
        }

        public static float GradiansToDegrees(float gradian)
        {
            return gradian * (9.0f / 10.0f);
        }

        public static float GradiansToRadians(float gradian)
        {
            return gradian * (Pi / 200.0f);
        }

        public static float RadiansToDegrees(float radian)
        {
            return radian * (180.0f / Pi);
        }

		public static Vector2 HeadingToDirectionScreenCoords(float heading)
		{
			float x = (float)Math.Cos(heading);
			float y = -(float)Math.Sin(heading);

			return new Vector2(x, y);
		}

		public static float DirectionScreenCoordsToHeading(Vector2 direction)
		{
			float fHeading = (float)Math.Atan2(-direction.Y, direction.X);

			if (fHeading < 0.0f)
				fHeading = TwoPi - (Math.Abs(fHeading) % TwoPi);
			else if (fHeading > 1.0f)
				fHeading = fHeading % TwoPi;

			return fHeading;
		}

		public static float HeadingToScreenAngle(float heading)
		{
			if (heading < 0.0f)
				heading = TwoPi - (Math.Abs(heading) % TwoPi);
			else if (heading > 1.0f)
				heading = heading % TwoPi;

			return -heading - PiOverTwo;
		}

		public static float Clamp(float value, float min, float max)
        {
            return value < min ? min : value > max ? max : value;
        }

        public static int Clamp(int value, int min, int max)
        {
            return value < min ? min : value > max ? max : value;
        }

        public static double Lerp(double from, double to, double amount)
        {
            return (1 - amount) * from + amount * to;
        }

        public static float Lerp(float from, float to, float amount)
        {
            return (1 - amount) * from + amount * to;
        }

        public static byte Lerp(byte from, byte to, float amount)
        {
            return (byte)Lerp((float)from, (float)to, amount);
        }

        public static float SmoothStep(float amount)
        {
            return (amount <= 0) ? 0
                : (amount >= 1) ? 1
                : amount * amount * (3 - (2 * amount));
        }

        public static float SmootherStep(float amount)
        {
            return (amount <= 0) ? 0
                : (amount >= 1) ? 1
                : amount * amount * amount * (amount * ((amount * 6) - 15) + 10);
        }

        public static float Mod(float value, float modulo)
        {
            if (modulo == 0.0f)
            {
                return value;
            }

            return value % modulo;
        }

        public static float Mod2PI(float value)
        {
            return Mod(value, TwoPi);
        }

        public static int Wrap(int value, int min, int max)
        {
            if (min > max)
                throw new ArgumentException(string.Format("min {0} should be less than or equal to max {1}", min, max), "min");

            // Code from http://stackoverflow.com/a/707426/1356325
            int range_size = max - min + 1;

            if (value < min)
                value += range_size * ((min - value) / range_size + 1);

            return min + (value - min) % range_size;
        }

        public static float Wrap(float value, float min, float max)
        {
            if (NearEqual(min, max)) return min;

            double mind = min;
            double maxd = max;
            double valued = value;

            if (mind > maxd)
                throw new ArgumentException(string.Format("min {0} should be less than or equal to max {1}", min, max), "min");

            var range_size = maxd - mind;
            return (float)(mind + (valued - mind) - (range_size * Math.Floor((valued - mind) / range_size)));
        }

        public static float Gauss(float amplitude, float x, float y, float centerX, float centerY, float sigmaX, float sigmaY)
        {
            return (float)Gauss((double)amplitude, x, y, centerX, centerY, sigmaX, sigmaY);
        }

        public static double Gauss(double amplitude, double x, double y, double centerX, double centerY, double sigmaX, double sigmaY)
        {
            var cx = x - centerX;
            var cy = y - centerY;

            var componentX = (cx * cx) / (2 * sigmaX * sigmaX);
            var componentY = (cy * cy) / (2 * sigmaY * sigmaY);

            return amplitude * Math.Exp(-(componentX + componentY));
        }
    }
}
