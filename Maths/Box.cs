﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maths
{
    [Serializable]
    public class Box : ICloneable
    {
        private Vector2 _center;
		/// <summary>
		/// Center of the box
		/// </summary>
		public Vector2 Center
        {
            get { return _center; }
            set
            {
                _center = value;
                Initialize();
            }
        }

        private Vector2 _size;
		/// <summary>
		/// Total size of the box (X = width, Y = height)
		/// </summary>
		public Vector2 Size
        {
            get { return _size; }
            set
            {
                _size = value;
                Initialize();
            }
        }

        private float _orientation;
        /// <summary>
		/// Orientation of the box in radian
		/// </summary>
		public float Orientation
        {
            get { return _orientation; }
            set
            {
                _orientation = value;
                Initialize();
            }
        }

        Vector2[] _corners = new Vector2[4];
        /// <summary>
		/// All four corners position of the box
		/// </summary>
		public ReadOnlyCollection<Vector2> Corners
        {
            get { return Array.AsReadOnly<Vector2>(_corners); }
        }

        Vector2[] _axis = new Vector2[2];
        float[] _origin = new float[2];

        /// <summary>
		/// Default constructor
		/// </summary>
		public Box()
        {
        }

        /// <summary>
		/// Full constructor
		/// </summary>
		/// <param name="center">Center of the box</param>
		/// <param name="size">Size of the box</param>
		/// <param name="orientation">Orientation of the box in radian</param>
		public Box(Vector2 center, Vector2 size, float orientation)
        {
            Center = center;
            Size = size;
            Orientation = orientation;

            Initialize();
        }

        /// <summary>
		/// Intersection test with two boxes
		/// </summary>
		/// <param name="first">The first box</param>
		/// <param name="second">The second box</param>
		/// <returns>true if there is an intersection, false otherwise</returns>
		public static bool Intersect(Box first, Box second)
        {
            return IntersectWithOther(first, second) && IntersectWithOther(second, first);
        }

		/// <summary>
		/// Intersection test with two boxes
		/// </summary>
		/// <param name="other">The other box to test against this one</param>
		/// <returns>true if there is an intersection, false otherwise</returns>
		public bool Intersect(Box other)
        {
            return IntersectWithOther(this, other) && IntersectWithOther(other, this);
        }

		/// <summary>
		/// Intersection test with a box and a point
		/// </summary>
		/// <param name="box">The box to test against</param>
		/// <param name="point">The point location to test against the box</param>
		/// <returns>true if there is an intersection, false otherwise</returns>
		public static bool Intersect(Box box, Vector2 point)
        {
            Vector2 diff = point - box.Center;
            return Math.Abs(Vector2.Dot(diff, box._axis[0])) <= 0.5f && Math.Abs(Vector2.Dot(diff, box._axis[1])) <= 0.5f;
        }

		/// <summary>
		/// Intersection test with a box and a point
		/// </summary>
		/// <param name="point">The point location to test against this box</param>
		/// <returns>true if there is an intersection, false otherwise</returns>
		public bool Intersect(Vector2 point)
        {
            return Intersect(this, point);
        }

		/// <summary>
		/// Intersection test with a box and a line
		/// </summary>
		/// <param name="box">The box to test the line against</param>
		/// <param name="line">The line to test an intersection</param>
		/// <returns>true if there is an intersection, false otherwise</returns>
		public static bool Intersect(Box box, Line line)
        {
            if (Intersect(box, line.P0) || Intersect(box, line.P1))
                return true;

            if (Line.Intersect(line.P0, line.P1, box._corners[0], box._corners[1]) ||
                Line.Intersect(line.P0, line.P1, box._corners[1], box._corners[2]) ||
                Line.Intersect(line.P0, line.P1, box._corners[2], box._corners[3]) ||
                Line.Intersect(line.P0, line.P1, box._corners[3], box._corners[0]))
                return true;

            return false;
        }

		/// <summary>
		/// Raycast between a box and a line to have the intersection point
		/// </summary>
		/// <param name="box">The box to raycast to</param>
		/// <param name="line">The line defining the ray for raycast</param>
		/// <param name="intersectPt">Output the intersection point if intersection was found or positive infinity otherwise</param>
		/// <returns>true if there is an intersection, false otherwise</returns>
		public static bool Raycast(Box box, Line line, out Vector2 intersectPt)
        {
            Vector2[] intersectPts = new Vector2[4];
            int closest = -1;
            float distance = float.MaxValue;

            for (int i=0; i<4; i++)
            {
                if (Line.Intersect(line.P0, line.P1, box._corners[i], box._corners[(i + 1) % 4], out intersectPts[i]))
                {
                    float localDist = (intersectPts[i] - line.P0).LengthSquared();
                    if (localDist < distance)
                    {
                        distance = localDist;
                        closest = i;
                    }
                }
            }

            if (closest >= 0)
            {
                intersectPt = intersectPts[closest];
                return true;
            }

            intersectPt = new Vector2(float.PositiveInfinity, float.PositiveInfinity);
            return false;
        }

		/// <summary>
		/// Raycast between a box and a line to have the intersection point
		/// </summary>
		/// <param name="line">The line defining the ray for raycasting this box</param>
		/// <param name="intersectPt">Output the intersection point if intersection was found or positive infinity otherwise</param>
		/// <returns>true if there is an intersection, false otherwise</returns>
		public bool Raycast(Line line, out Vector2 intersectPt)
        {
            return Raycast(this, line, out intersectPt);
        }

		/// <summary>
		/// Intersection test with this box and a line
		/// </summary>
		/// <param name="line">The line to test intersection with this box</param>
		/// <returns>true if there is an intersection, false otherwise</returns>
		public bool Intersect(Line line)
        {
            return Intersect(this, line);
        }

		/// <summary>
		/// Intersection test between a box and a triangle
		/// </summary>
		/// <param name="box">The box to test with the triangle</param>
		/// <param name="triangle">The triangle to test against the box</param>
		/// <returns>true if there is an intersection, false otherwise</returns>
		public static bool Intersect(Box box, Triangle triangle)
        {
            if (Intersect(box, triangle.P0) || Intersect(box, triangle.P1) || Intersect(box, triangle.P2))
                return true;

            Line[] edges = new Line[3];
            for (int i = 0; i < 3; i++)
                if (Intersect(box, edges[i]))
                    return true;

            return false;
        }

		/// <summary>
		/// Intersection test between a box and a triangle
		/// </summary>
		/// <param name="triangle">The triangle to test against this box</param>
		/// <returns>true if there is an intersection, false otherwise</returns>
		public bool Intersect(Triangle triangle)
        {
            return Intersect(this, triangle);
        }

        private void Initialize()
        {
            double ori = Orientation;
            Vector2 X = new Vector2((float)Math.Cos(ori), (float)Math.Sin(ori));
            Vector2 Y = new Vector2((float)-Math.Sin(ori), (float)Math.Cos(ori));

            X *= Size.X / 2.0f;
            Y *= Size.Y / 2.0f;

            _corners[0] = Center - X - Y;
            _corners[1] = Center + X - Y;
            _corners[2] = Center + X + Y;
            _corners[3] = Center - X + Y;

            ComputeAxis();
        }

        private void ComputeAxis()
        {
            _axis[0] = _corners[1] - _corners[0];
            _axis[1] = _corners[3] - _corners[0];

            // Make the length of each axis 1/edge length so we know any
            // dot product must be less than 1 to fall within the edge.
            for (int i = 0; i < 2; i++)
            {
                _axis[i] /= _axis[i].LengthSquared();
                _origin[i] = Vector2.Dot(_corners[0], _axis[i]);
            }
        }

        private static bool IntersectWithOther(Box first, Box second)
        {
            for (int i = 0; i < 2; i++)
            {
                double t = Vector2.Dot(second._corners[0], first._axis[i]);

                // Find the extent of box 2 on axis a
                double tMin = t;
                double tMax = t;

                for (int j = 1; j < 4; j++)
                {
                    t = Vector2.Dot(second._corners[j], first._axis[i]);

                    if (t < tMin)
                        tMin = t;
                    else if (t > tMax)
                        tMax = t;
                }

                // We have to subtract off the origin
                // See if [tMin, tMax] intersects [0, 1]
                if ((tMin > 1 + first._origin[i]) || (tMax < first._origin[i]))
                {
                    // There was no intersection along this dimension;
                    // the boxes cannot possibly overlap.
                    return false;
                }
            }

            // There was no dimension along which there is no intersection.
            // Therefore the boxes overlap.
            return true;
        }

		/// <summary>
		/// Clone this object
		/// </summary>
		/// <returns>Returns a copy of the box</returns>
		public object Clone()
		{
			return new Box(Center, Size, Orientation);
		}
	}
}
