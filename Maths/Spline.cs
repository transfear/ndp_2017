﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maths
{
    struct SearchStruct
    {
        public SearchStruct(int idx, float dist)
        {
            index = idx;
            distSq = dist;
        }

        public int index;
        public float distSq;
    }

    [Serializable]
    public class Spline : ICloneable
    {
		/// <summary>
		/// Array containing the list of all control point position of spline
		/// </summary>
		public Vector2[] Points;

		/// <summary>
		/// Array containing the list of all control point tangents of spline
		/// </summary>
		public Vector2[] Tans;
        public Spline(Vector2[] points, Vector2[] tans)
        {
            Points = points;
            Tans = tans;
        }

        /// <summary>
		/// Clone the spline
		/// </summary>
		/// <returns>A copy of the spline</returns>
		public object Clone()
        {
            return Utils.DeepClone<Spline>(this);
        }

        /// <summary>
		/// Evaluate the position on the spline given a specific time
		/// </summary>
		/// <param name="t">Time parameter to evaluate the spline. 
		/// 0 is the starting point and 1 is the end point of the spline.</param>
		/// <returns>The evaluated position on the spline</returns>
		public Vector2 Eval(float t)
        {
			if (t < 0.0f)
				t = 1.0f - (Math.Abs(t) % 1.0f);
			else if (t > 1.0f)
				t = t % 1.0f;

			int nbSegments = Points.Length - 1;
            int p0 = (int)Math.Floor(t * nbSegments);
            int p1 = p0 + 1;
            p0 = Math.Max(p0, 0);
			p1 = Math.Min(p1, nbSegments);

            t = (t * (float)nbSegments) % 1.0f;
            float t2 = t * t;
            float t3 = t2 * t;

            Vector2 P0 = Points[p0];
            Vector2 P1 = Points[p1];
            Vector2 T0 = Tans[p0];
            Vector2 T1 = Tans[p1];

            float h1 = 2.0f * t3 - 3.0f * t2 + 1.0f;
            float h2 = -2.0f * t3 + 3.0f * t2;
            float h3 = t3 - 2.0f * t2 + t;
            float h4 = t3 - t2;

            return h1 * P0 + h2 * P1 + h3 * T0 + h4 * T1;
        }

		/// <summary>
		/// Evaluate the tangent on the spline given a specific time
		/// </summary>
		/// <param name="t">Time parameter to evaluate the spline. 
		/// 0 is the starting point and 1 is the end point of the spline.</param>
		/// <returns>The evaluated tangent vector at given time on the spline</returns>
		public Vector2 EvalTangent(float t)
        {
			if (t < 0.0f)
				t = 1.0f - (Math.Abs(t) % 1.0f);
			else if (t > 1.0f)
				t = t % 1.0f;

			int nbSegments = GetSegmentCount();
            int p0 = (int)Math.Floor(t * nbSegments);
            int p1 = p0 + 1;
            p0 = Math.Max(p0, 0);
			p1 = Math.Min(p1, nbSegments);

            t = (t * (float)nbSegments) % 1.0f;
            float t2 = t * t;

            Vector2 P0 = Points[p0];
            Vector2 P1 = Points[p1];
            Vector2 T0 = Tans[p0];
            Vector2 T1 = Tans[p1];

            float h1 = 6.0f * t2 - 6.0f * t;
            float h2 = -6.0f * t2 + 6.0f * t;
            float h3 = 3.0f * t2 - 4.0f * t + 1.0f;
            float h4 = 3.0f * t2 - 2.0f * t;

            Vector2 tangent = h1 * P0 + h2 * P1 + h3 * T0 + h4 * T1;
            tangent.Normalize();

            return tangent;
        }

		/// <summary>
		/// Gets the segment on the spline given a specific time
		/// </summary>
		/// <param name="time">Time parameter to evaluate the spline. 
		/// 0 is the starting point and 1 is the end point of the spline.</param>
		/// <returns>The segment index which holds the given time.</returns>
		public int GetSegmentFromTime(float time)
		{
			time = time % 1.0f;
			int nbSegments = GetSegmentCount();
			int segment = (int)Math.Floor(time * nbSegments);
			return segment;
		}

		/// <summary>
		/// Gets the total amount of segment in spline.
		/// </summary>
		/// <returns>The segment count of the spline.</returns>
		public int GetSegmentCount()
		{
			return Points.Length - 1;
		}

		/// <summary>
		/// Evaluate the closest time on the spline from a given position.
		/// </summary>
		/// <param name="pt">The point where to find the time.</param>
		/// <returns>The time closest to the given point.</returns>
		public float GetClosestTimeToPoint(Vector2 pt)
		{
			int scans = GetSegmentCount(); //First and last are the same
			Vector2 vec = Points[0];

            //Find squared distance from all points
            List<SearchStruct> search = new List<SearchStruct>();
			for (int i = 0; i <= scans; i++)
			{
				Vector2 evalPt = Eval((float)i / scans);
				float d2 = Vector2.DistanceSquared(pt, evalPt);
                search.Add(new SearchStruct(i, d2));
            }

            //Sort them ascending
            List<SearchStruct> ordered = search.OrderBy(x => x.distSq).ToList();

            //Take the 3 closest points and do the search
            int maxCount = Math.Min(ordered.Count, 3);
            float min = float.MaxValue;
            float minDist = float.MaxValue;
            for (int i = 0; i < maxCount; i++)
            {
                //Get the lower and upper bound from found index
                float t0 = (float)(ordered[i].index - 1) / scans;
                float t1 = (float)(ordered[i].index + 1) / scans;

                //Find the closest point in between bounds (local minimum)
                float time = GetLocalMinimum(pt, t0, t1, 1e-4f);

                Vector2 evalPt = Eval(time);
                float d2 = Vector2.DistanceSquared(pt, evalPt);

                //Keep the closest point found yet
                if (d2 < minDist)
                {
                    minDist = d2;
                    min = time;
                }
            }

            // Normalize time
            if (min < 0.0f)
                min += 1.0f;
            if (min > 1.0f)
                min -= 1.0f;

            return min;
		}

		float GetLocalMinimum(Vector2 pt, float minX, float maxX, float epsilon)
		{
			float low = minX;
			float high = maxX;
			float time = 0.0f;
			//Do the binary search between the given intervals
			while ((high - low) > epsilon)
			{
				time = (high + low) * 0.5f;

				Vector2 lowPt = Eval(time - epsilon);
				Vector2 highPt = Eval(time + epsilon);
				float lowD2 = Vector2.DistanceSquared(pt, lowPt);
				float highD2 = Vector2.DistanceSquared(pt, highPt);

				if (lowD2 < highD2)
					high = time;
				else
					low = time;
			}

			return time;
		}
	}
}
