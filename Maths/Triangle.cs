﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maths
{
    [Serializable]
    public class Triangle : IEquatable<Triangle>
    {
		/// <summary>
		/// Triangle constructor
		/// </summary>
		/// <param name="p0">First vertex coordinate of triangle</param>
		/// <param name="p1">Second vertex coordinate of triangle</param>
		/// <param name="p2">Third vertex coordinate of triangle</param>
		public Triangle(Vector2 p0, Vector2 p1, Vector2 p2)
        {
            P0 = p0;
            P1 = p1;
            P2 = p2;
        }

        public static bool operator ==(Triangle left, Triangle right)
        {
            if ((object)left == null || (object)right == null)
                return false;

            return left.Equals(ref right);
        }

        public static bool operator !=(Triangle left, Triangle right)
        {
            if ((object)left == null || (object)right == null)
                return true;

            return !left.Equals(ref right);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + P0.GetHashCode();
                hash = hash * 23 + P1.GetHashCode();
                hash = hash * 23 + P2.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is Triangle)
                return this.Equals((Triangle)obj);
            else
                return false;
        }

        public bool Equals(Triangle other)
        {
            return P0 == other.P0 && P1 == other.P1 && P2 == other.P2;
        }

        public bool Equals(ref Triangle other)
        {
            return P0 == other.P0 && P1 == other.P1 && P2 == other.P2;
        }

        /// <summary>
		/// Intersection test between a triangle and a point, allows to know if a point is inside a triangle
		/// </summary>
		/// <param name="pt">The point to test with the triangle</param>
		/// <returns>Returns true if point is contained in triangle, false otherwise</returns>
		public bool Intersect(Vector2 pt)
        {
            const float epsilon = 0.0001f;
            const float inferiorTest = 0.0f - epsilon;
            const float superiorTest = 1.0f + epsilon;

            float x1 = P0.X, x2 = P1.X, x3 = P2.X;
            float y1 = P0.Y, y2 = P1.Y, y3 = P2.Y;
            float x = pt.X;
            float y = pt.Y;

            float denominator = (x1 * (y2 - y3) + y1 * (x3 - x2) + x2 * y3 - y2 * x3);
            float t1 = (x * (y3 - y1) + y * (x1 - x3) - x1 * y3 + y1 * x3) / denominator;
            float t2 = (x * (y2 - y1) + y * (x1 - x2) - x1 * y2 + y1 * x2) / -denominator;
            float s = t1 + t2;

            return inferiorTest <= t1 && t1 <= superiorTest && inferiorTest <= t2 && t2 <= superiorTest && s <= 1.0001f;
        }

        /// <summary>
		/// First coordinate of triangle vertex
		/// </summary>
		public Vector2 P0;
		/// <summary>
		/// Second coordinate of triangle vertex
		/// </summary>
		public Vector2 P1;
		/// <summary>
		/// Third coordinate of triangle vertex
		/// </summary>
		public Vector2 P2;
    }
}
