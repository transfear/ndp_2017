﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maths
{
    [Serializable]
    public class Line : ICloneable
    {
        /// <summary>
		/// The start point of the line
		/// </summary>
		public Vector2 P0;

        /// <summary>
		/// The end point of the line
		/// </summary>
		public Vector2 P1;

		/// <summary>
		/// Line constructor
		/// </summary>
		/// <param name="pt0x">Start point X coordinate of the line</param>
		/// <param name="pt0y">Start point Y coordinate of the line</param>
		/// <param name="pt1x">End point X coordinate of the line</param>
		/// <param name="pt1y">End point Y coordinate of the line</param>
		public Line(float pt0x, float pt0y, float pt1x, float pt1y)
        {
            P0.X = pt0x;
            P0.Y = pt0y;
            P1.X = pt1x;
            P1.Y = pt1y;
        }

		/// <summary>
		/// Line constructor
		/// </summary>
		/// <param name="p0">Start point of the line</param>
		/// <param name="p1">End point of the line</param>
		public Line(Vector2 p0, Vector2 p1)
        {
            P0 = p0;
            P1 = p1;
        }

        /// <summary>
		/// Intersection test between this line and the line represented by two points
		/// </summary>
		/// <param name="p0">Start point of the line</param>
		/// <param name="p1">End point of the line</param>
		/// <returns>Returns true if there is intersection, false otherwise</returns>
		public bool Intersect(Vector2 p0, Vector2 p1)
        {
            Vector2 intersectionPt;
            return Intersect(new Line(p0, p1), out intersectionPt);
        }

		/// <summary>
		/// Intersection test between this line and the line represented by two points
		/// </summary>
		/// <param name="p0">Start point of the line</param>
		/// <param name="p1">End point of the line</param>
		/// <param name="intersectionPt">Returns the intersection point</param>
		/// <returns>Returns true if there is intersection, false otherwise</returns>
		public bool Intersect(Vector2 p0, Vector2 p1, out Vector2 intersectionPt)
        {
            return Intersect(new Line(p0, p1), out intersectionPt);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="line"></param>
		/// <returns>Returns true if there is intersection, false otherwise</returns>
		public bool Intersect(Line line)
        {
            return Intersect(this, line);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="line"></param>
		/// <param name="intersectionPt">Returns the intersection point</param>
		/// <returns>Returns true if there is intersection, false otherwise</returns>
		public bool Intersect(Line line, out Vector2 intersectionPt)
        {
            return Intersect(this, line, out intersectionPt);
        }

		/// <summary>
		/// Intersection test between two lines
		/// </summary>
		/// <param name="line0">First line to test intersection<</param>
		/// <param name="line1">Second line to test intersection</param>
		/// <returns>Returns true if there is intersection, false otherwise</returns>
		public static bool Intersect(Line line0, Line line1)
        {
            return Intersect(line0.P0, line0.P1, line1.P0, line1.P1);
        }

		/// <summary>
		/// Intersection test between two lines
		/// </summary>
		/// <param name="line0">First line to test intersection</param>
		/// <param name="line1">Second line to test intersection</param>
		/// <param name="intersectionPt">Returns the intersection point</param>
		/// <returns>Returns true if there is intersection, false otherwise</returns>
		public static bool Intersect(Line line0, Line line1, out Vector2 intersectionPt)
        {
            return Intersect(line0.P0, line0.P1, line1.P0, line1.P1, out intersectionPt);
        }

		/// <summary>
		/// Intersection test between two lines
		/// </summary>
		/// <param name="p00">Start point of first line</param>
		/// <param name="p01">End point of first line</param>
		/// <param name="p10">Start point of second line</param>
		/// <param name="p11">End point of second line</param>
		/// <returns>Returns true if there is intersection, false otherwise</returns>
		public static bool Intersect(Vector2 p00, Vector2 p01, Vector2 p10, Vector2 p11)
        {
            Vector2 intersect;
            return Intersect(p00, p01, p10, p11, out intersect);
        }

		/// <summary>
		/// Intersection test between two lines
		/// </summary>
		/// <param name="p00">Start point of first line</param>
		/// <param name="p01">End point of first line</param>
		/// <param name="p10">Start point of second line</param>
		/// <param name="p11">End point of second line</param>
		/// <param name="intersectionPt">Returns the intersection point</param>
		/// <returns>Returns true if there is intersection, false otherwise</returns>
		public static bool Intersect(Vector2 p00, Vector2 p01, Vector2 p10, Vector2 p11, out Vector2 intersectionPt)
        {
            float epsilon = 0.0001f;
            float inferiorBound = 0.0f - epsilon;
            float superiorBound = 1.0f + epsilon;

            Vector2 p0Delta = p01 - p00;
            Vector2 p1Delta = p11 - p10;

            float s, t;
            s = (-p0Delta.Y * (p00.X - p10.X) + p0Delta.X * (p00.Y - p10.Y)) / (-p1Delta.X * p0Delta.Y + p0Delta.X * p1Delta.Y);
            t = (p1Delta.X * (p00.Y - p10.Y) - p1Delta.Y * (p00.X - p10.X)) / (-p1Delta.X * p0Delta.Y + p0Delta.X * p1Delta.Y);

            if (s >= inferiorBound && s <= superiorBound && t >= inferiorBound && t <= superiorBound)
            {
                intersectionPt.X = p00.X + t * p0Delta.X;
                intersectionPt.Y = p00.Y + t * p0Delta.Y;

                return true;
            }

            intersectionPt.X = float.PositiveInfinity;
            intersectionPt.Y = float.PositiveInfinity;

            return false;
        }

		/// <summary>
		/// Clone this object
		/// </summary>
		/// <returns>Returns a copy of the line</returns>
		public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
