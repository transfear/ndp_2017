﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maths
{
    [Serializable]
    public class QuadTree
    {
        public static QuadTree Instance = null;

        public int Depth;
        public AxisAlignedBox Bounds;
        public List<Triangle> Triangles = new List<Triangle>();
        public List<QuadTree> Childrens = null;

        public static void InitializeInstance(AxisAlignedBox bounds, int depth)
        {
            if (Instance == null)
            {
                Instance = new QuadTree(bounds, depth);
            }
        }

        public static void ShutdownInstance()
        {
            Instance = null;
        }

        public static List<Triangle> GetAllTriangles()
        {
            if (Instance != null)
                return Instance.Triangles;

            return null;
        }

        private QuadTree(AxisAlignedBox bounds, int depth)
        {
            if (Instance == null)
            {
                Bounds = bounds;
                Depth = depth;
                CreateChilds(depth - 1);
            }
        }

        private void CreateChilds(int depth)
        {
            if (depth > 0)
            {
                Childrens = new List<QuadTree>();

                float childWidth = Bounds.Width / 2.0f;
                float childHeight = Bounds.Height / 2.0f;

                //Top left
                Childrens.Add(new QuadTree(new AxisAlignedBox(Bounds.Left, Bounds.Top, Bounds.Left + childWidth, Bounds.Top + childHeight), depth));

                //Top right
                Childrens.Add(new QuadTree(new AxisAlignedBox(Bounds.Left + childWidth, Bounds.Top, Bounds.Right, Bounds.Top + childHeight), depth));

                //Bot left
                Childrens.Add(new QuadTree(new AxisAlignedBox(Bounds.Left, Bounds.Top + childHeight, Bounds.Left + childWidth, Bounds.Bottom), depth));

                //Bot right
                Childrens.Add(new QuadTree(new AxisAlignedBox(Bounds.Left + childWidth, Bounds.Top + childHeight, Bounds.Right, Bounds.Bottom), depth));
            }
        }

        public void AddTriangles(List<Triangle> triangles)
        {
            foreach (Triangle triangle in triangles)
            {
                AddTriangleRecurse(this, triangle);
            }
        }

        private void AddTriangleRecurse(QuadTree quadtree, Triangle triangle)
        {
            if (quadtree.Bounds.Intersect(triangle))
            {
                quadtree.Triangles.Add(triangle);

                if (quadtree.Childrens != null)
                {
                    foreach (QuadTree child in quadtree.Childrens)
                    {
                        AddTriangleRecurse(child, triangle);
                    }
                }
            }
        }

        public void PruneEmptyNodes(QuadTree quadtree)
        {
            if (quadtree.Childrens == null)
                return;

            List<QuadTree> toRemove = new List<QuadTree>();
            //For each children find those to remove
            foreach (QuadTree child in quadtree.Childrens)
            {
                if (child.Triangles.Count == 0)
                    toRemove.Add(child);
            }

            //Remove them
            foreach (QuadTree child in toRemove)
            {
                quadtree.Childrens.Remove(child);
            }

            //Process children for additional nodes to remove
            foreach (QuadTree child in quadtree.Childrens)
            {
                PruneEmptyNodes(child);
            }
        }

        public List<Triangle> GetTrianglesFromPoint(Vector2 point)
        {
            return GetTrianglesFromPointRecurse(this, point);
        }

        static private List<Triangle> GetTrianglesFromPointRecurse(QuadTree quadtree, Vector2 point)
        {
            if (quadtree.Bounds.Intersect(point))
            {
                if (quadtree.Childrens != null)
                {
                    foreach (QuadTree child in quadtree.Childrens)
                    {
                        List<Triangle> tris = GetTrianglesFromPointRecurse(child, point);
                        if (tris != null && tris.Count > 0)
                            return tris;
                    }
                }

                return quadtree.Triangles;
            }

            return null;
        }
    }
}
