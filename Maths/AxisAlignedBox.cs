﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maths
{
    [Serializable]
    public class AxisAlignedBox
    {
        public AxisAlignedBox(float left, float top, float right, float bottom)
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;

            TopLeft = new Vector2(Left, Top);
            TopRight = new Vector2(Right, Top);
            BottomLeft = new Vector2(Left, Bottom);
            BottomRight = new Vector2(Right, Bottom);
        }

        static bool Intersect(AxisAlignedBox box, Vector2 point)
        {
            if (point.X >= box.Left && point.X <= box.Right && point.Y >= box.Top && point.Y <= box.Bottom)
                return true;

            return false;
        }

        public bool Intersect(Vector2 point)
        {
            return Intersect(this, point);
        }

        static bool Intersect(AxisAlignedBox box, Triangle triangle)
        {
            // Is a triangle vertex in rect
            if (Intersect(box, triangle.P0) || Intersect(box, triangle.P1) || Intersect(box, triangle.P2))
                return true;

            // Is a rect vertex in triangle
            if (triangle.Intersect(box.TopLeft) || triangle.Intersect(box.TopRight) || triangle.Intersect(box.BottomLeft) || triangle.Intersect(box.BottomRight))
                return true;

            // Intersect all triangle edges with rectangle edges
            if (Line.Intersect(triangle.P0, triangle.P1, box.TopLeft, box.TopRight) ||
                Line.Intersect(triangle.P0, triangle.P1, box.TopRight, box.BottomLeft) ||
                Line.Intersect(triangle.P0, triangle.P1, box.BottomLeft, box.BottomRight) ||
                Line.Intersect(triangle.P0, triangle.P1, box.BottomRight, box.TopRight) ||
                Line.Intersect(triangle.P1, triangle.P2, box.TopLeft, box.TopRight) ||
                Line.Intersect(triangle.P1, triangle.P2, box.TopRight, box.BottomLeft) ||
                Line.Intersect(triangle.P1, triangle.P2, box.BottomLeft, box.BottomRight) ||
                Line.Intersect(triangle.P1, triangle.P2, box.BottomRight, box.TopRight) ||
                Line.Intersect(triangle.P2, triangle.P0, box.TopLeft, box.TopRight) ||
                Line.Intersect(triangle.P2, triangle.P0, box.TopRight, box.BottomLeft) ||
                Line.Intersect(triangle.P2, triangle.P0, box.BottomLeft, box.BottomRight) ||
                Line.Intersect(triangle.P2, triangle.P0, box.BottomRight, box.TopRight))
                return true;

            return false;
        }

        public bool Intersect(Triangle triangle)
        {
            return Intersect(this, triangle);
        }

        public float Left;
        public float Top;
        public float Right;
        public float Bottom;

        public Vector2 TopLeft;
        public Vector2 TopRight;
        public Vector2 BottomLeft;
        public Vector2 BottomRight;

        public float Width
        {
            get { return Right - Left; }
            set
            {
                Right = Left + value;
            }
        }

        public float Height
        {
            get { return Bottom - Top; }
            set
            {
                Bottom = Top + value;
            }
        }
    }
}
