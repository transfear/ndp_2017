﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Renderer_NDP2016.ComponentModel;

namespace Renderer_NDP2016.ViewModel
{
    public class RendererVM : ObservableObject
    {
        ObservableCollection<PlayerVM> _players = new ObservableCollection<PlayerVM>();
        public ObservableCollection<PlayerVM> Players
        {
            get { return _players; }
            set
            {
                _players = value;
                RaisePropertyChanged();
            }
        }

        int _totalLaps = 0;
        public int TotalLaps
        {
            get { return _totalLaps; }
            set
            {
                _totalLaps = value;
                RaisePropertyChanged();
            }
        }

        TimeSpan _totalTime = new TimeSpan(0);
        public TimeSpan TotalTime
        {
            get { return _totalTime; }
            set
            {
                _totalTime = value;
                RaisePropertyChanged();
            }
        }

		bool _showDebugLines = true;
		public bool ShowDebugLines
		{
			get { return _showDebugLines; }
			set
			{
				_showDebugLines = value;
				RaisePropertyChanged();
			}
		}

		bool _showClosestPointFromPlayers = false;
		public bool ShowClosestPointFromPlayers
		{
			get { return _showClosestPointFromPlayers; }
			set
			{
				_showClosestPointFromPlayers = value;
				RaisePropertyChanged();
			}
		}

		bool _showNavMesh = false;
		public bool ShowNavMesh
		{
			get { return _showNavMesh; }
			set
			{
				_showNavMesh = value;
				RaisePropertyChanged();
			}
		}

		bool _showQuadTree = false;
		public bool ShowQuadTree
		{
			get { return _showQuadTree; }
			set
			{
				_showQuadTree = value;
				RaisePropertyChanged();
			}
		}

		bool _showSpline = true;
		public bool ShowSpline
		{
			get { return _showSpline; }
			set
			{
				_showSpline = value;
				RaisePropertyChanged();
			}
		}

		bool _showTrackPolygons = false;
		public bool ShowTrackPolygons
		{
			get { return _showTrackPolygons; }
			set
			{
				_showTrackPolygons = value;
				RaisePropertyChanged();
			}
		}

		bool _showBoundingBoxes = false;
		public bool ShowBoundingBoxes
		{
			get { return _showBoundingBoxes; }
			set
			{
				_showBoundingBoxes = value;
				RaisePropertyChanged();
			}
		}
	}
}
