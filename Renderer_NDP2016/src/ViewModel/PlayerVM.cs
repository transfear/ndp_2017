﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renderer_NDP2016.ComponentModel;

namespace Renderer_NDP2016.ViewModel
{
    public class PlayerVM : ObservableObject
    {
        uint _index = 0;
        public uint Index
        {
            get { return _index; }
            set
            {
                _index = value;
                RaisePropertyChanged();
            }
        }

        string _name = "Name";
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged();
            }
        }

        public float PedalHeight
        {
            get { return 40.0f; }
        }

        int _position = 0;
        public int Position
        {
            get { return _position; }
            set
            {
				_position = value;
                RaisePropertyChanged();
            }
        }

		public int PositionFontSize
		{
			get
			{
				return Math.Max((6 - Position), 0) * 2 + 20;
			}
		}

        float _throttle = 0.0f;
        public float Throttle
        {
            get { return _throttle; }
            set
            {
                _throttle = value * PedalHeight;
                RaisePropertyChanged();
                RaisePropertyChanged("ThrottleOffset");
            }
        }

        public float ThrottleOffset
        {
            get { return PedalHeight - Throttle; }
        }

        float _brake = 0.0f;
        public float Brake
        {
            get { return _brake; }
            set
            {
                _brake = value * PedalHeight;
                RaisePropertyChanged();
                RaisePropertyChanged("BrakeOffset");
            }
        }

        public float BrakeOffset
        {
            get { return PedalHeight - Brake; }
        }

        float _steering = 0.0f;
        public float Steering
        {
            get { return _steering; }
            set
            {
                _steering = value;
                RaisePropertyChanged();
            }
        }

        string _lap = "";
        public string Lap
        {
            get { return _lap; }
            set
            {
                _lap = value;
                RaisePropertyChanged();
            }
        }

        int _ammo = 0;
        public int Ammo
        {
            get { return _ammo; }
            set
            {
                _ammo = value;
                RaisePropertyChanged();
            }
        }

        string _simulationTime = "";
        public string SimulationTime
        {
            get { return _simulationTime; }
            set
            {
                _simulationTime = value;
                RaisePropertyChanged();
            }
        }

		System.Windows.Media.SolidColorBrush _carColor = System.Windows.Media.Brushes.White;
		public System.Windows.Media.SolidColorBrush CarColor
		{
			get { return _carColor; }
			set
			{
				_carColor = value;
				RaisePropertyChanged();
			}
		}
	}
}
