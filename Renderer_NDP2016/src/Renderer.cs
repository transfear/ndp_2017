﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

using Simulation_NDP2016.Data;
using FrameworkInterface.Renderer;
using FrameworkInterface.Simulation;
using System.Windows.Threading;

namespace Renderer_NDP2016
{
	public class Renderer : IRenderer
	{
		AutoResetEvent    mWindowSetEvent = new AutoResetEvent(false);
        UI.RendererWindow mWindow         = null;
		UIThread          mUIThread       = null;

		public IRendererFeedback OnGameStart(ISimulationOutput _simulationData)
		{
            SimulationOutput simOutput = _simulationData as SimulationOutput;
			if (simOutput == null)
				return null;

			bool bFirstStart = mUIThread == null;
			if (bFirstStart)
			{
				mUIThread = new UIThread(this);
				mUIThread.StartUIThread(simOutput);
				mWindowSetEvent.WaitOne();
			}

			mWindow.CloseRequested = false;
			return null;
		}

        public void SetWindow(UI.RendererWindow _window)
		{
            ViewModel.RendererVM vm = new ViewModel.RendererVM();

			mWindow = _window;
            mWindow.DataContext = vm;

            mWindowSetEvent.Set();
		}

		public IRendererFeedback Update(ISimulationOutput _simulationData)
		{
            SimulationOutput simOutput = _simulationData as SimulationOutput;
            if (simOutput == null)
                return null;

			// Create feedback for the simulation
			RendererFeedback feedback = new RendererFeedback();
            feedback.ShouldQuit = false;

            if (mWindow != null)
            {
                const float steeringAngleMultiplier = 4.0f;

                feedback.ShouldQuit = mWindow.CloseRequested;
                feedback.ShouldRestart = false;

				// Feed new simulation output to renderer, the window will use this data when it renders
				// We lock them so render thread and update thread aren't updating/using it at the same time
				lock (mWindow.rendererLockObject)
				{
					mWindow.simOutput = simOutput;
				}

				// Update image and view model
				mWindow.Dispatcher.BeginInvoke((Action)(() =>
                {
					// Update image is now down in the RendererWindow.xaml since it wasn't double buffering properly.
					// This effectively moves the rendering process to the WPF render thread which will free a decent amount
					// Of resources for player simulation
					// Update image
					// mWindow.UpdateUI(simOutput);

                    List<SimulationOutput.PlayerData> orderedPlayers = simOutput.players.OrderByDescending(x => x.lap).ThenByDescending(x => simOutput.track.CenterLine.GetClosestTimeToPoint(x.curPos)).ToList();

                    // Update view model
                    ViewModel.RendererVM vm = mWindow.DataContext as ViewModel.RendererVM;
                    foreach (SimulationOutput.PlayerData playerData in simOutput.players)
                    {
                        bool bFound = false;

                        foreach (ViewModel.PlayerVM playerVM in vm.Players)
                        {
                            if (playerVM.Index == playerData.player.PlayerIndex)
                            {
                                for (int i=0; i<orderedPlayers.Count; i++)
                                {
                                    if (orderedPlayers[i].player.PlayerIndex == playerData.player.PlayerIndex)
                                    {
                                        playerVM.Position = i + 1;
                                        break;
                                    }
                                }
								
                                playerVM.Throttle = playerData.fThrottle;
                                playerVM.Brake = playerData.fBrake;
                                playerVM.Steering = Maths.Utils.RadiansToDegrees(playerData.fSteering * steeringAngleMultiplier);
								playerVM.CarColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(playerData.color.A, playerData.color.R, playerData.color.G, playerData.color.B));
								if (playerData.lap <= simOutput.totalLaps)
                                    playerVM.Lap = playerData.lap.ToString() + " / " + simOutput.totalLaps.ToString();
                                else
                                    playerVM.Lap = playerData.finishTime.ToString("m':'ss':'fff");
                                playerVM.Ammo = (int)playerData.uiAmmo;

                                float ms = (float)((double)playerData.turnTime.Ticks / (double)TimeSpan.TicksPerMillisecond);
                                playerVM.SimulationTime = String.Format("{0:0.0000}", ms) + " ms";

                                bFound = true;
                                break;
                            }
                        }

                        if (!bFound)
                        {
                            ViewModel.PlayerVM playerVM = new ViewModel.PlayerVM();

                            for (int i = 0; i < orderedPlayers.Count; i++)
                            {
                                if (orderedPlayers[i].player.PlayerIndex == playerData.player.PlayerIndex)
                                {
                                    playerVM.Position = i + 1;
                                    break;
                                }
                            }

                            playerVM.Name = playerData.player.GetPlayerName();
							playerVM.Index = playerData.player.PlayerIndex;
                            playerVM.Throttle = playerData.fThrottle;
                            playerVM.Brake = playerData.fBrake;
                            playerVM.Steering = Maths.Utils.RadiansToDegrees(playerData.fSteering * steeringAngleMultiplier);
							playerVM.CarColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(playerData.color.A, playerData.color.R, playerData.color.G, playerData.color.B));
                            if (playerData.lap <= simOutput.totalLaps)
                                playerVM.Lap = playerData.lap.ToString() + " / " + simOutput.totalLaps.ToString();
                            else
                                playerVM.Lap = playerData.finishTime.ToString("m':'ss':'fff");

                            vm.Players.Add(playerVM);
                        }
                    }

                    vm.TotalLaps = (int)simOutput.totalLaps;
                    vm.TotalTime = simOutput.totalTime;
                }),System.Windows.Threading.DispatcherPriority.Render);
			}

			return feedback;
        }

		public IRendererFeedback OnGameEnd(ISimulationOutput _simulationData)
		{
            RendererFeedback newFeedBack = new RendererFeedback();
			newFeedBack.ShouldQuit    = true;
			newFeedBack.ShouldRestart = false;

            SimulationOutput simOutput = _simulationData as SimulationOutput;
			if (simOutput != null)
			{
                string leaderboard = "";
                IOrderedEnumerable< SimulationOutput.PlayerData> players = simOutput.players.OrderBy(x => x.finishTime);
                bool hasPlayers = players.Count() > 0;
                foreach (SimulationOutput.PlayerData curPlayer in players)
				{
                    leaderboard += curPlayer.player.GetPlayerName() + " : " + curPlayer.finishTime.ToString("m':'ss':'fff") + "\n";
                }

				if (hasPlayers)
				{
                    string message = "Race is finish, the winner is " + players.First().player.GetPlayerName() + "\n\nTime results:\n" + leaderboard;
                    message += "\n\nWould you like to restart?";

                    Application.Current.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                    {
                        MessageBoxResult msgResult = MessageBox.Show(mWindow, message, "Race finished!", MessageBoxButton.YesNo);
                        newFeedBack.ShouldRestart = msgResult == MessageBoxResult.Yes;
                    }));
				}
			}

			// Close the window
			mWindow.Dispatcher.Invoke((Action)(() => 
			{ 
				// Explicit shutdown
				if (!newFeedBack.ShouldRestart)
				{
					mWindow.Close();
					mUIThread.ExplicitShutdown();
				}
			}));

			// Wait for the thread to finish
			if (!newFeedBack.ShouldRestart)
				mUIThread.Join();

			return newFeedBack;
		}
	}
}
