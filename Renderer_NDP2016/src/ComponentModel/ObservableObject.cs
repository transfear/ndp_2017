﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace Renderer_NDP2016.ComponentModel
{
    public class ObservableObject : INotifyPropertyChanged
    {
        protected static string GetPropertyName<T>(Expression<Func<T>> action)
        {
            var expression = (MemberExpression)action.Body;
            var propertyName = expression.Member.Name;
            return propertyName;
        }

        protected void RaisePropertyChanging<T>(Expression<Func<T>> action)
        {
            var propertyName = GetPropertyName(action);
            RaisePropertyChanging(propertyName);
        }

        protected void RaisePropertyChanged<T>(Expression<Func<T>> action)
        {
            var propertyName = GetPropertyName(action);
            RaisePropertyChanged(propertyName);
        }

        protected void RaisePropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void RaisePropertyChanging([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanging != null)
                PropertyChanging(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void ChangeProperty<T>(ref T var, T value, [CallerMemberName]string propertyName = null)
        {
            RaisePropertyChanging(propertyName);
            var = value;
            RaisePropertyChanged(propertyName);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangedEventHandler PropertyChanging;
    }
}
