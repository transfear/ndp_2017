﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using FrameworkInterface.Player;
using Simulation_NDP2016.Data;
using Renderer_NDP2016.Data;
using Renderer_NDP2016.Utilities;

namespace Renderer_NDP2016.UI
{
	/// <summary>
	/// Interaction logic for RendererWindow.xaml
	/// </summary>
	public partial class RendererWindow : Window
	{
		public bool CloseRequested { get; set; }

		public SimulationOutput simOutput { get; set; }

		public object rendererLockObject = new object();

		public RendererWindow()
		{
			InitializeComponent();

            RenderOptions.SetBitmapScalingMode(imgGrid, BitmapScalingMode.NearestNeighbor);

			CloseRequested = false;
			Closing += OnWindowClosing;

			CompositionTarget.Rendering += CompositionTarget_Rendering;
		}

		private void CompositionTarget_Rendering(object sender, EventArgs e)
		{
			lock(rendererLockObject)
			{
				if (simOutput != null)
					UpdateUI(simOutput);
			}
		}

		UInt32 mGridWidth  = 1;
        UInt32 mGridHeight = 1;

        Dictionary<IPlayer, PlayerData> mPlayerMap = new Dictionary<IPlayer, PlayerData>();
        Random mRNG = new Random();

		public void OnGameStart(SimulationOutput _simOutput)
		{
            mPlayerMap.Clear();

            // Generate a random number generator with a fixed seed based on player input
			UInt32 randomSeed = PlayerData.GenerateRandomSeed(_simOutput.players);
            mRNG = new Random((int)randomSeed);

            // Populate our player map
            foreach (SimulationOutput.PlayerData curPlayer in _simOutput.players)
                mPlayerMap[curPlayer.player] = new PlayerData();

            // Assign random color to players
            ColorUtils.AssignRandomColor(mRNG, mPlayerMap);

            // Setup track image
            mGridWidth  = _simOutput.uiGridWidth;
            mGridHeight = _simOutput.uiGridHeight;
            ImageSource mainObjective = BitmapUtils.CreateWBFromSimulation(mGridWidth, mGridHeight, _simOutput, this.DataContext as ViewModel.RendererVM);
            imgGrid.Source = mainObjective;
		}

        public void UpdateUI(SimulationOutput _simOutput)
		{
            // Update simulation
            ImageSource mainObjective = BitmapUtils.CreateWBFromSimulation(mGridWidth, mGridHeight, _simOutput, this.DataContext as ViewModel.RendererVM);
            imgGrid.Source = mainObjective;
		}

		private void OnWindowClosing(object sender, CancelEventArgs e)
		{
			// Always prevent the window from closing - let the simulation decide what to do
			CloseRequested = true;
			e.Cancel = true;
		}
	}
}
