﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Simulation_NDP2016.Data;
using Maths;
using System.Runtime.InteropServices;

namespace Renderer_NDP2016.Utilities
{
    static class BitmapUtils
	{
		static bool ShowClosestPointFromPlayers = false;
		static bool ShowPolygons = false;
		static bool ShowSpline = false;
        static bool ShowRayCast = false;
		static bool ShowNavMesh = false;
        static bool ShowQuadTree = false;
        static bool ShowQuadTreeWithTimer = false;
        static bool ShowDebugLines = false;
		static bool ShowBoundingBoxes = false;

		static bool RedrawMap = false;

        static void DrawSpline(Graphics graphics, System.Drawing.Color color, Spline spline, int nbSteps)
        {
            List<PointF> pointList = new List<PointF>();
            for (int i = 0; i <= nbSteps; i++)
            {
                float t = (float)i / (float)(nbSteps);
                Vector2 p0 = spline.Eval(t);

                pointList.Add(new PointF(p0.X, p0.Y));
            }

            System.Drawing.Pen pen = new System.Drawing.Pen(color);

            pen.Width = 3.0F;
            pen.DashCap = System.Drawing.Drawing2D.DashCap.Flat;
            pen.DashPattern = new float[] { 4.0F, 8.0F };

            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            graphics.DrawLines(pen, pointList.ToArray());
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;

            // Dispose of the custom pen.
            pen.Dispose();
        }

        static void DrawSpline(Graphics graphics, System.Drawing.Brush brush, Spline spline, int nbSteps, float width)
        {
            Vector2 p0 = spline.Eval(0.0f);
            Vector2 p1;
            Vector2 t0 = spline.EvalTangent(0.0f);
            Vector2 t1;

            float antialiasingBorder = 1.0f; // 1 px border to prevent AA glitch
            float halfWidth = width * 0.5f;

            for (int i = 1; i <= nbSteps; i++)
            {
                float t = (float)i / (float)(nbSteps);
                p1 = spline.Eval(t);
                t1 = spline.EvalTangent(t);

                Vector2 p00 = Vector2.PerpendicularClockwise(t0);
                p00.Normalize();
                p00 = p0 + (p00 * halfWidth) - (t0 * antialiasingBorder);

                Vector2 p01 = Vector2.PerpendicularCounterClockwise(t0);
                p01.Normalize();
                p01 = p0 + (p01 * halfWidth) - (t0 * antialiasingBorder);

                Vector2 p10 = Vector2.PerpendicularCounterClockwise(t1);
                p10.Normalize();
                p10 = p1 + (p10 * halfWidth) + (t1 * antialiasingBorder);

                Vector2 p11 = Vector2.PerpendicularClockwise(t1);
                p11.Normalize();
                p11 = p1 + (p11 * halfWidth) + (t1 * antialiasingBorder);

                PointF[] pts = new PointF[4]
                {
                    new PointF(p00.X, p00.Y),
                    new PointF(p01.X, p01.Y),
                    new PointF(p10.X, p10.Y),
                    new PointF(p11.X, p11.Y),
                };

                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                graphics.FillPolygon(brush, pts);

				if (ShowPolygons)
				{
					graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
					graphics.DrawPolygon(System.Drawing.Pens.White, pts);
				}

				p0 = p1;
                t0 = t1;
            }

            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
        }

        static void DrawStartLine(Graphics graphics, System.Drawing.Color color1, System.Drawing.Color color2, Spline spline, float time, float width, float depth)
        {
            Vector2 p0 = spline.Eval(time);
            Vector2 t0 = spline.EvalTangent(time);

            float halfWidth = width * 0.5f;
            float halfDepth = depth * 0.5f;
            Vector2 halfDepthTan = t0 * halfDepth;

            Vector2 p00 = Vector2.PerpendicularClockwise(t0);
            p00 = p0 + (p00 * halfWidth);

            Vector2 p01 = Vector2.PerpendicularCounterClockwise(t0);
            p01 = p0 + (p01 * halfWidth);

            PointF[] pts = new PointF[2]
            {
                new PointF(p00.X, p00.Y),
                new PointF(p01.X, p01.Y),
            };

            System.Drawing.Pen pen1 = new System.Drawing.Pen(color1);
            pen1.Width = depth;
            pen1.DashCap = System.Drawing.Drawing2D.DashCap.Flat;
            pen1.DashPattern = new float[] { 2.0F, 2.0F };

            System.Drawing.Pen pen2 = new System.Drawing.Pen(color2);
            pen2.Width = depth;
            pen2.DashCap = System.Drawing.Drawing2D.DashCap.Flat;
            pen2.DashPattern = new float[] { 2.0F, 2.0F };
            pen2.DashOffset = 2.0f;

            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphics.DrawLine(pen1, pts[0], pts[1]);
            graphics.DrawLine(pen2, pts[0], pts[1]);

            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
        }

		static void DrawBox(Graphics graphics, System.Drawing.Pen pen, Box box)
		{
			graphics.DrawLine(pen, box.Corners[0].X, box.Corners[0].Y, box.Corners[1].X, box.Corners[1].Y);
			graphics.DrawLine(pen, box.Corners[1].X, box.Corners[1].Y, box.Corners[2].X, box.Corners[2].Y);
			graphics.DrawLine(pen, box.Corners[2].X, box.Corners[2].Y, box.Corners[3].X, box.Corners[3].Y);
			graphics.DrawLine(pen, box.Corners[3].X, box.Corners[3].Y, box.Corners[0].X, box.Corners[0].Y);
		}

		static Bitmap trackImage = null;
        static UInt32 trackID = 0;
        static Bitmap CreateTrackImage(uint width, uint height, Track track)
        {
            if (trackImage != null && track.ID == trackID && !RedrawMap)
                return trackImage;

			RedrawMap = false;
			if (trackImage != null)
            {
                trackImage.Dispose();
                trackImage = null;
            }

            Bitmap trackBmp = new Bitmap((int)width, (int)height, System.Drawing.Imaging.PixelFormat.Format24bppRgb); ;

            var graphics = System.Drawing.Graphics.FromImage(trackBmp);
            graphics.Clear(System.Drawing.Color.Black);

            // Draw grass
            DrawSpline(graphics, System.Drawing.Brushes.DarkGreen, track.CenterLine, track.DrawSteps, track.GrassWidth);

            // Draw road
            DrawSpline(graphics, System.Drawing.Brushes.DarkGray, track.CenterLine, track.DrawSteps, track.RoadWidth);

            // Draw middle spline
            if (ShowSpline)
                DrawSpline(graphics, System.Drawing.Color.White, track.CenterLine, track.DrawSteps);

            // Draw start line
            DrawStartLine(graphics, System.Drawing.Color.White, System.Drawing.Color.Black, track.CenterLine, track.StartLine, track.RoadWidth, 3.0f);

            if (ShowNavMesh)
            {
                List<Triangle> mesh = QuadTree.GetAllTriangles();
                for (int i = 0; i < mesh.Count; i++)
                {
                    graphics.DrawLine(System.Drawing.Pens.White, mesh[i].P0.X, mesh[i].P0.Y, mesh[i].P1.X, mesh[i].P1.Y);
                    graphics.DrawLine(System.Drawing.Pens.White, mesh[i].P2.X, mesh[i].P2.Y, mesh[i].P1.X, mesh[i].P1.Y);
                    graphics.DrawLine(System.Drawing.Pens.White, mesh[i].P0.X, mesh[i].P0.Y, mesh[i].P2.X, mesh[i].P2.Y);
                }
            }

            if (ShowQuadTree)
            {
                BuildQuadTreeList(QuadTree.Instance);

                foreach(QuadTree tree in quadTreeList)
                {
                    graphics.DrawLine(System.Drawing.Pens.Red, tree.Bounds.TopLeft.X, tree.Bounds.TopLeft.Y, tree.Bounds.TopRight.X, tree.Bounds.TopRight.Y);
                    graphics.DrawLine(System.Drawing.Pens.Red, tree.Bounds.TopRight.X, tree.Bounds.TopRight.Y, tree.Bounds.BottomRight.X, tree.Bounds.BottomRight.Y);
                    graphics.DrawLine(System.Drawing.Pens.Red, tree.Bounds.BottomRight.X, tree.Bounds.BottomRight.Y, tree.Bounds.BottomLeft.X, tree.Bounds.BottomLeft.Y);
                    graphics.DrawLine(System.Drawing.Pens.Red, tree.Bounds.BottomLeft.X, tree.Bounds.BottomLeft.Y, tree.Bounds.TopLeft.X, tree.Bounds.TopLeft.Y);
                }
            }

            trackImage = trackBmp;
            trackID = track.ID;

            return trackImage;
        }

        static public BitmapSource ConvertToSource(System.Drawing.Bitmap bitmap)
        {
            var bitmapData = bitmap.LockBits(
                new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);

            var bitmapSource = BitmapSource.Create(bitmapData.Width, bitmapData.Height,
                bitmap.HorizontalResolution, bitmap.VerticalResolution,
                PixelFormats.Bgr24, null,
                bitmapData.Scan0, bitmapData.Stride * bitmapData.Height, bitmapData.Stride);

            bitmap.UnlockBits(bitmapData);
            return bitmapSource;
        }

        public static ImageSource CreateWBFromSimulation(uint width, uint height, SimulationOutput simulation, ViewModel.RendererVM rendererVM)
		{
			if (displayPlayerIdxTimer == null)
			{
				displayPlayerIdxTimer = new System.Timers.Timer(200.0f);
				displayPlayerIdxTimer.Elapsed += DisplayPlayerIdxTimer_Elapsed;
				displayPlayerIdxTimer.Start();
			}

			if (rendererVM != null)
			{
				ShowDebugLines = rendererVM.ShowDebugLines;
				ShowClosestPointFromPlayers = rendererVM.ShowClosestPointFromPlayers;
				ShowBoundingBoxes = rendererVM.ShowBoundingBoxes;
				if (ShowSpline != rendererVM.ShowSpline)
				{
					ShowSpline = rendererVM.ShowSpline;
					RedrawMap = true;
				}
				if (ShowNavMesh != rendererVM.ShowNavMesh)
				{
					ShowNavMesh = rendererVM.ShowNavMesh;
					RedrawMap = true;
				}
				if (ShowQuadTree != rendererVM.ShowQuadTree)
				{
					ShowQuadTree = rendererVM.ShowQuadTree;
					RedrawMap = true;
				}
				if (ShowPolygons != rendererVM.ShowTrackPolygons)
				{
					ShowPolygons = rendererVM.ShowTrackPolygons;
					RedrawMap = true;
				}
			}

			Track track = simulation.track;
            Bitmap trackBmp = CreateTrackImage(width, height, track);
            Bitmap bmp = trackBmp.Clone() as Bitmap;

            Graphics graphics = Graphics.FromImage(bmp);
			graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            // Draw obstacles
            foreach (TrackObject obstacle in simulation.obstacles)
            {
                System.Drawing.Brush fillColor;
                System.Drawing.Pen contourColor;

                switch (obstacle.objectType)
                {
                    case TrackObjectType.Boost:
                        fillColor = System.Drawing.Brushes.DarkRed;
                        contourColor = new System.Drawing.Pen(new SolidBrush(System.Drawing.Color.FromArgb(64, 0, 0)));
                        break;
                    case TrackObjectType.Bounce:
                        fillColor = new SolidBrush(System.Drawing.Color.FromArgb(16, 16, 16));
                        contourColor = new System.Drawing.Pen(new SolidBrush(System.Drawing.Color.FromArgb(0, 0, 0)));
                        break;
                    case TrackObjectType.PowerUp:
                        fillColor = System.Drawing.Brushes.Yellow;
                        contourColor = new System.Drawing.Pen(new SolidBrush(System.Drawing.Color.FromArgb(128, 128, 0)));
                        break;
                    default:
                        fillColor = System.Drawing.Brushes.Red;
                        contourColor = Pens.Black;
                        break;
                }

				PointF[] poly = new PointF[4];
				for (int i = 0; i < 4; i++)
				{
					poly[i] = new PointF(obstacle.Box.Corners[i].X, obstacle.Box.Corners[i].Y);
				}

				graphics.FillPolygon(fillColor, poly, System.Drawing.Drawing2D.FillMode.Alternate);

                // Draw arrow on booster
                if (obstacle.objectType == TrackObjectType.Boost)
                {
                    System.Drawing.Pen arrowPen = new System.Drawing.Pen(System.Drawing.Color.FromArgb(255, 48, 48));
                    graphics.DrawLine(arrowPen,
                        obstacle.Box.Corners[1].X,
                        obstacle.Box.Corners[1].Y,
                        obstacle.Box.Corners[2].X + (obstacle.Box.Corners[3].X - obstacle.Box.Corners[2].X) * 0.5f,
                        obstacle.Box.Corners[2].Y + (obstacle.Box.Corners[3].Y - obstacle.Box.Corners[2].Y) * 0.5f);

                    graphics.DrawLine(arrowPen,
                        obstacle.Box.Corners[2].X + (obstacle.Box.Corners[3].X - obstacle.Box.Corners[2].X) * 0.5f,
                        obstacle.Box.Corners[2].Y + (obstacle.Box.Corners[3].Y - obstacle.Box.Corners[2].Y) * 0.5f,
                        obstacle.Box.Corners[0].X,
                        obstacle.Box.Corners[0].Y);
                }

                graphics.DrawPolygon(contourColor, poly);

				if (ShowBoundingBoxes)
				{
					DrawBox(graphics, System.Drawing.Pens.White, obstacle.Box);
				}
			}

            // Draw projectiles
            foreach (Projectile projectile in simulation.projectiles)
            {
                graphics.FillEllipse(new SolidBrush(System.Drawing.Color.FromArgb(0, 128, 0)),
					projectile.trackProjectile.Box.Center.X - projectile.trackProjectile.Box.Size.X / 2,
					projectile.trackProjectile.Box.Center.Y - projectile.trackProjectile.Box.Size.X / 2,
					projectile.trackProjectile.Box.Size.X,
					projectile.trackProjectile.Box.Size.Y);
				graphics.DrawEllipse(new System.Drawing.Pen(System.Drawing.Color.FromArgb(0, 64, 0)), 
					projectile.trackProjectile.Box.Center.X - projectile.trackProjectile.Box.Size.X / 2, 
					projectile.trackProjectile.Box.Center.Y - projectile.trackProjectile.Box.Size.X / 2, 
					projectile.trackProjectile.Box.Size.X, 
					projectile.trackProjectile.Box.Size.Y);

				if (ShowBoundingBoxes)
				{
					DrawBox(graphics, System.Drawing.Pens.White, projectile.trackProjectile.Box);
				}
			}

			// Draw players
			Int32 numPlayers = simulation.players.Count();
			int playerLoop = 0;
			while(playerLoop < numPlayers)
            {
				int playerIdx = (startDisplayPlayerIdx + playerLoop) % numPlayers;

				const float carWidth = 8.0f;    // @see BaseSimulation.CarData.carSize.X
                const float carLength = 12.0f;  // @see BaseSimulation.CarData.carSize.Y

                SimulationOutput.PlayerData playerData = simulation.players[playerIdx];
                System.Drawing.Color color = System.Drawing.Color.FromArgb(playerData.color.R, playerData.color.G, playerData.color.B);

                // Setup car transform
				graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
                graphics.ResetTransform();
                graphics.TranslateTransform(playerData.curPos.X, playerData.curPos.Y);
                graphics.RotateTransform(Utils.RadiansToDegrees(Utils.HeadingToScreenAngle(playerData.fHeading)));

                // Draw car
                graphics.FillRectangle(new SolidBrush(color), -carWidth / 2.0f, -carLength / 2.0f, carWidth, carLength);

                // Draw head lights
                graphics.FillRectangle(System.Drawing.Brushes.White, -(carWidth / 2.0f) + 0.0f, +(carLength / 2.0f) - 2.0f, 2.0f, 2.0f);
                graphics.FillRectangle(System.Drawing.Brushes.White, +(carWidth / 2.0f) - 2.0f, +(carLength / 2.0f) - 2.0f, 2.0f, 2.0f);

                // Draw tail lights (dark if not breaking, light if breaking)
                System.Drawing.Brush brakeColor = playerData.fBrake > 0.0f ? new SolidBrush(System.Drawing.Color.FromArgb(255, 32, 32)) : System.Drawing.Brushes.DarkRed;
                graphics.FillRectangle(brakeColor, -(carWidth / 2.0f) + 0.0f, -(carLength / 2.0f), 2.0f, 2.0f);
                graphics.FillRectangle(brakeColor, +(carWidth / 2.0f) - 2.0f, -(carLength / 2.0f), 2.0f, 2.0f);

                graphics.ResetTransform();

				if (ShowBoundingBoxes)
				{
					DrawBox(graphics, System.Drawing.Pens.White, playerData.boundingBox);
				}

				if (ShowClosestPointFromPlayers)
				{
					graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
					float time = track.CenterLine.GetClosestTimeToPoint(playerData.curPos);
					Vector2 closestPt = track.CenterLine.Eval(time);
					graphics.DrawLine(System.Drawing.Pens.LightCyan, closestPt.X, closestPt.Y, playerData.curPos.X, playerData.curPos.Y);
				}

				if (ShowRayCast)
                {
                    Vector2 carDirection = Utils.HeadingToDirectionScreenCoords(playerData.fHeading);
                    carDirection.Normalize();

                    Vector2 carPos = playerData.curPos;

                    Vector2 intersectPos = Vector2.Zero;

                    //Line line = new Line(carPos, carPos + carDirection * 100.0f);
                    //TrackObjectType hit = track.Raycast(TrackObjectType.Wall | TrackObjectType.Bounce, simulation.obstacles, line, out intersectPos);

                    TrackObjectType hit = track.Raycast(TrackObjectType.Wall | TrackObjectType.Bounce, simulation.obstacles, carPos, carDirection, out intersectPos);
                    if (hit != TrackObjectType.None)
                        graphics.DrawLine(System.Drawing.Pens.Aqua, carPos.X, carPos.Y, intersectPos.X, intersectPos.Y);
                }

                if (ShowQuadTreeWithTimer)
                { 
                    if (quadTreeTimer == null)
                    {
                        BuildQuadTreeList(QuadTree.Instance);

                        quadTreeTimer = new System.Timers.Timer(200.0f);
                        quadTreeTimer.Elapsed += Timer_Elapsed;
                        quadTreeTimer.Start();
                    }

                    int value = quadTreeIndex % quadTreeList.Count;
                    QuadTree tree = quadTreeList[value];

                    graphics.DrawLine(System.Drawing.Pens.Orange, tree.Bounds.TopLeft.X, tree.Bounds.TopLeft.Y, tree.Bounds.TopRight.X, tree.Bounds.TopRight.Y);
                    graphics.DrawLine(System.Drawing.Pens.Orange, tree.Bounds.TopRight.X, tree.Bounds.TopRight.Y, tree.Bounds.BottomRight.X, tree.Bounds.BottomRight.Y);
                    graphics.DrawLine(System.Drawing.Pens.Orange, tree.Bounds.BottomRight.X, tree.Bounds.BottomRight.Y, tree.Bounds.BottomLeft.X, tree.Bounds.BottomLeft.Y);
                    graphics.DrawLine(System.Drawing.Pens.Orange, tree.Bounds.BottomLeft.X, tree.Bounds.BottomLeft.Y, tree.Bounds.TopLeft.X, tree.Bounds.TopLeft.Y);

                    List<Triangle> mesh = tree.Triangles;
                    for (int i = 0; i < mesh.Count; i++)
                    {
                        graphics.DrawLine(System.Drawing.Pens.DarkOrange, mesh[i].P0.X, mesh[i].P0.Y, mesh[i].P1.X, mesh[i].P1.Y);
                        graphics.DrawLine(System.Drawing.Pens.DarkOrange, mesh[i].P2.X, mesh[i].P2.Y, mesh[i].P1.X, mesh[i].P1.Y);
                        graphics.DrawLine(System.Drawing.Pens.DarkOrange, mesh[i].P0.X, mesh[i].P0.Y, mesh[i].P2.X, mesh[i].P2.Y);
                    }
                }

                if (ShowDebugLines)
                {
                    if (playerData.debugLines != null)
                    {
                        foreach(Line line in playerData.debugLines)
                        {
                            graphics.DrawLine(Pens.DarkRed, line.P0.X, line.P0.Y, line.P1.X, line.P1.Y);
                        }
                    }
                }

				playerLoop++;
			}

            graphics.ResetTransform();
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;

            graphics.Dispose();
            graphics = null;

            BitmapSource bmpSrc = ConvertToSource(bmp);
            bmpSrc.Freeze();

            bmp.Dispose();
            bmp = null;

            return bmpSrc;
        }

		static int startDisplayPlayerIdx = 0;
		static System.Timers.Timer displayPlayerIdxTimer = null;
		private static void DisplayPlayerIdxTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			startDisplayPlayerIdx++;
		}

		static List<QuadTree> quadTreeList = null;
        static int quadTreeIndex = 0;
        private static void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            quadTreeIndex++;
        }

        static System.Timers.Timer quadTreeTimer = null;
        static public void BuildQuadTreeList(QuadTree tree)
        {
            if (quadTreeList == null)
            {
                quadTreeList = new List<QuadTree>();
            }

            quadTreeList.Add(tree);

            if (tree.Childrens != null)
            {
                foreach (QuadTree child in tree.Childrens)
                {
                    BuildQuadTreeList(child);
                }
            }
        }
    }
}
